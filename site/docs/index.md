---
title: Documentation
...

Always check [libreboot.org](https://libreboot.org/) for the latest updates to
libreboot. News, including release announcements, can be found in
the [main news section](../news/).

[Answers to Frequently Asked Questions about libreboot](../faq.md).

Need help?
==========

Help is available on [Libreboot IRC](../contact.md) and other channels.

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

Installing libreboot
====================

-   [What systems can I use libreboot on?](hardware/)
-   [How to install libreboot](install/)

Documentation related to operating systems
============================

-   [How to install BSD on an x86 host system](bsd/)
-   [Linux Guides](linux/)

Information for developers
==========================

-   [How to compile the libreboot source code](build/)
-   [Build system developer documentation](maintain/)
-   [GRUB payload](grub/)
-   [U-Boot payload](uboot/)

Other information
=================

-   [Miscellaneous](misc/)
-   [List of codenames](misc/codenames.md)

