---
title: Документація
...

Завжди перевіряйте [libreboot.org](https://libreboot.org/index.uk.html) для останніх оновлень
libreboot. Новини, включаючи оголошення про випуски, може бути знайдено
в [основній секції новин](../news/).

[Відповіді на поширені запитання про libreboot](../faq.md).

Need help?
----------

Help is available on [Libreboot IRC](../contact.md) and other channels.

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

Встановлення libreboot
----------------------

-   [Як встановити libreboot](install/)

Документація, яка має відношення до операційних систем
-----------------------------------------------------

-   [Як встановити BSD на x86 хостову систему](bsd/)
-   [Керівництва Linux](linux/)

Інформація для розробників
--------------------------

-   [Як зібрати джерельний код libreboot](build/)
-   [Документація розробника системи побудови](maintain/)
-   [Корисне навантаження GRUB](grub/)
-   [Корисне навантаження U-Boot](uboot/)

Інша інформація
---------------

-   [Libreboot Static Site Generator](sitegen/)
-   [Різне](misc/)
-   [Список кодових назв](misc/codenames.md)

