---
title: Flashing the Dell Latitude E6430
x-toc-enable: true
...

**NOTE:** This installation guide also pertains to E5520, E6420, E6520, E5530
and E6530.

Introduction
============

This guide is for those who want libreboot on their Latitude E6430 while
they still have the original Dell firmware present. This guide can also be
followed (adapted) if you brick your E6430, and you want to recover it.

**The Intel video initialisation is libre, implemented with publicly available
source code via libgfxinit, from the coreboot project.**

Only the intel GPU variants are supported. The Nvidia GPU variants are untested
and should be assumed *broken* by default.

Flash chip size {#flashchips}
===============

Use this to find out:

	flashprog -p internal

The internal flash size is 12MiB on this board; physically, an 8MiB and 4MiB
chip (two chips) but this distinction only matters for external flashing.

MAC address {#macaddress}
===========

The MAC address is part of the ROM image that you're flashing. You can change
it at any time, before or after you've flashed Libreboot; you can also change
it in the *Dell* BIOS, if you really want to. This is for the onboard gigabit
ethernet device.

Refer to [mac\_address.md](../hardware/mac_address.md).

It is recommended that you run *nvmutil*. See:

[nvmutil usage manual](nvmutil.md) - E5530 users don't need to run this.

The `nvmutil` software is specifically designed for changing MAC addresses,
and it implements a few more safeguards (e.g. prevents multicast/all-zero
MAC addresses) and features (MAC address randomisation, ability to correct or
intentionally corrupt(disable) GbE sections if you wish, swap GbE parts,
etc). - **it is also the only software available for this. Please do not
use ich9gen on this board. This is an ivybridge board, not ICH9M.**

Intel GPU: libre video initialisation available
===============================================

Libreboot uses coreboot's native `libgfxinit` on this platform, for
variants with Intel graphics.

Libreboot releases after 20230625 will support this board, otherwise you can
refer to the [build instructions](../build/)

Nvidia GPU: Video BIOS Option ROM required
==========================================

UNSUPPORTED. DO NOT BUY THE NVIDIA VARIANT.

Only the Intel GPU variants are supported. It is believed that the Nvidia
variants still have Intel GPUs in them, configured via hardware mux, but this
has not yet been configured or tested in the coreboot code; you should assume
that the Nvidia models do not work.

How to flash internally (no diassembly)
=======================================

Warning for BSD users
---------------------

**NOTE (15 October 2023): The util is now called `dell-flash-unlock`, but it
was previously called `e6400-flash-unlock`. Links have been updated.**

BSD *boots* and works properly on these machines, but take note:

Nicholas's [dell-flash-unlock](https://browse.libreboot.org/lbmk.git/plain/util/dell-flash-unlock/dell_flash_unlock.c)
utility has been ported to OpenBSD, but *other* BSDs are assumed unsupported for
now.

NOTE: Libreboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 27 January 2024, which is a fork of flashrom.

NOTE: BSD is mentioned above, but the only BSD tested for `dell-flash-unlock`
is OpenBSD, as of 15 October 2023.

Flashing from Linux
-------------------

MAKE SURE you boot with this Linux kernel parameter: `iomem=relaxed` - this
disables memory protections, permitting `/dev/mem` access needed by flashprog.
The flash is memory mapped and flashprog accesses it via `/dev/mem`.

You can flash Libreboot directly from the vendor (Dell) BIOS, without taking
the machine apart. It can be done entirely from Linux. It will probably also
work on BSD systems, but it has only been testing on Linux thus far.

**NOTE (15 October 2023): The util is now called `dell-flash-unlock`, but it
was previously called `e6400-flash-unlock`. Links have been updated.**

Check `util/dell-flash-unlock` in the `lbmk.git` repository, or in release
archives for Libreboot releases from 20230423 onwards.

Go in there:

	cd util/dell-flash-unlock
	make

With this program, you can unlock the flash in such a way where everything
is writeable. Information about how to use it is in the `README.md` file which
is included in that program's directory, or you can read it online here:

<https://browse.libreboot.org/lbmk.git/plain/util/dell-flash-unlock/README.md>

Literally just run that program, and do what it says. You run it once, shut
down, and then power on the machine. Then run it again to confirm that the
flash is unlocked. The source code is intuitive enough that you can easily get
the gist of it; it's writing some EC commands and changing some chipset config
bits. The EC on this machine is hooked up to the `HDA_SDO` signal, also known
as the Flash Descriptor Override (FDO), which disables any flash protection by
the IFD. When booted with the FDO set, the original Dell firmware disables all
other BIOS write protections such as SMM BIOS lock bits.

*Make sure* to make a backup of the original firmware before proceeding to
flash; see the instructions below.

When you flash it, you can use this command:

	flashprog -p internal -w libreboot.rom

Where `libreboot.rom` is your E6430 ROM. *Make sure* it's the right one.
If flashprog complains about multiple flash chips detected, just pick one of
them (doesn't matter which one), with the `-C` flag as directed; just pick one,
and if it doesn't work, try the next.

When you see flashprog say `VERIFIED` at the end, that means the flash was
successful. If you don't see that, or you're unsure, please [contact the
Libreboot project via IRC](../../contact.md).

BACK UP THE FACTORY BIOS
========================

The `-w` option flashes `libreboot.rom`. You may consider *backing up* the
original Dell BIOS first, using the -r option:

	flashprog -p internal -r backup.rom

AGAIN: make sure to use `-C` accordingly, if you need to pick a chip (where
flashprog has detected multiple chip definitions). Just pick one, any of them
will probably work, if not try the next.

Do this while in a flashable state, after the 2nd run of `dell-flash-unlock`.

Make sure the `backup.rom` file gets backed up to an external storage media,
not the E6430 itself.

With this method, you can probably flash it within 5 minutes. Again, zero
disassembly required!

How to flash externally
=========================

Refer to [spi.md](spi.md) as a guide for external re-flashing.

The SPI flash chip shares a voltage rail with the chipset, which is not
isolated using a diode. As a result, powering the flash chip externally may
cause the QM77 chipset to partially power up and drive the SPI pins, which can
interfere with programmers such as the Raspberry Pi. See [RPi Drive
Strength](spi.md#rpi-drive-strength) for a workaround.

Have a look online for videos showing how to disassemble, if you wish to
externally re-flash.

