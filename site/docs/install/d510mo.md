---
title: D510MO flashing tutorial 
...

This guide is for those who want libreboot on their Intel D510MO
motherboard while they still have the original BIOS present.

NOTE: D410PT is another designation and it's the same board. Flash the same ROM.

Flash chip size {#flashchips}
===============

Use this to find out:

	flashprog -p internal

NOTE: Libreboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 27 January 2024, which is a fork of flashrom.

Flashing instructions {#clip}
=====================

Refer to [spi.md](spi.md) for how to re-flash externally.
