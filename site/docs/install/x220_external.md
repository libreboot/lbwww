---
title: ThinkPad X220/X220T external flashing
x-toc-enable: true
...

**[PLEASE READ THESE INSTRUCTIONS BEFORE INSTALLING](../../news/safety.md),
OR YOU MIGHT BRICK YOUR MACHINE: [SAFETY PRECAUTIONS](../../news/safety.md)**

Make sure to read the [Ivybridge/Haswell common guide](/docs/install/ivy_has_common.html) before attempting to flash this board.
After you have prepared a rom and split it into two section, refer to this guide for disassembly instructions.

We don't currently have disassembly instructions for this board.
See [coreboot docs](https://www.coreboot.org/Board:lenovo/x220) for how to disassemble this machine to reveal the flash chips.
