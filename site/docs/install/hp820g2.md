---
title: Install Libreboot on HP EliteBook 820 G2
x-toc-enable: true
...

**[PLEASE READ THESE INSTRUCTIONS BEFORE INSTALLING](ivy_has_common.md), OR
YOU MAY BRICK YOUR MACHINE!! - Please click the link and follow the instructions
there, before flashing. For posterity,
[here is the link again](ivy_has_common.md).**

<div class="specs">
<center>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2.jpg" /></span>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_lid.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_lid.jpg" /></span>
<br/>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_ports1.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_ports1.jpg" /></span>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_ports2.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_ports2.jpg" /></span>
<br/>
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | HP                                             |
| **Name**                   | EliteBook 820 G2                               |
| **Variants**               | EliteBook 820 G2                               |
| **Released**               | 2014                                           |
| **Chipset**                | 5th gen (Broadwell, SoC)                       |
| **CPU**                    | Intel i5-5200U, i5-5300U, i7-5500U, i7-5600U   |
| **Graphics**               | Intel HD 5500 graphics (libre initialisation)  |
| **Display**                | 14" 1366x768 or 1920x1080 TFT                  |
| **Memory**                 | Two slots, max 16GB/slot (32GB), DDR3/SODIMM   |
| **Architecture**           | x86_64                                         |
| **EC**                     | SMSC MEC1324 in main boot flash                |
| **Original boot firmware** | HP UEFI firmware                               |
| **Intel ME/AMD PSP**       | Present. Can be disabled with me_cleaner.      |
| **Flash chip**             | SOIC-8 16MiB 128Mbit, 12MiB usable by coreboot |


```
W+: Works without vendor firmware;
N: Doesn't work;
W*: Works with vendor firmware;
U: Untested;
P+: Partially works;
P*: Partially works with vendor firmware
```

| ***Features***                                    |    |
|---------------------------------------------------|----|
| **Internal flashing with original boot firmware** | N  |
| **Display (if Intel GPU)**                        | W+ |
| **Audio**                                         | W+ |
| **RAM Init**                                      | W+ |
| **External output**                               | W+ |
| **Display brightness**                            | W+ |

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**                  | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB**     | Works     |
</div>

Open source BIOS/UEFI firmware
-------------------------

This document will teach you how to install Libreboot, on your
HP EliteBook 820 G2 laptop motherboard.
Libreboot is a [Free Software](https://writefreesoftware.org/learn) project
that replaces proprietary BIOS/UEFI firmware.

Please build this from source with lbmk:

	./mk -b coreboot hp820g2_12mb

More information is available in the [build guide](../build/), including how
to install build dependencies.

Alternatively, you can use pre-compiled release images.

Full hardware specifications can be found on HP's own website:

<https://support.hp.com/gb-en/document/c04543492>

Pre-compiled images now possible!
--------------------------------

From Libreboot 20241206 **revision 8** onwards, pre-compiled ROM images *are*
now available. Previous releases excluded images for this board, because vendor
file insertion was not reproducible, so you would get checksum errors. This
has been fixed with the following patch:

<https://browse.libreboot.org/lbmk.git/commit/?id=e8799310db26df89720e8476a701f1904932234b>

The refcode is inserted uncompressed, whereas upstream uses LZMA compression.
We can't predict how the implementation will change in the future, and any
behavioural changes would probably affect the checksum on insertion. Older
releases also didn't handle `rmodtool` on refcode insertion, which is used
to make the file relocatable in CBFS. See:

<https://doc.coreboot.org/lib/rmodules.html>

As of Libreboot 20241206 rev8, you can now use pre-compiled release images
and [insert vendor files](ivy_has_common.md).

The lack of refcode compression costs about 110KB, because the refcode file is
about 180KB uncompressed, but would be about 70KB compressed in flash. We
insert it uncompressed, so it's 180KB in flash. This is a small sacrifice,
considering that you still have about 10MB of unused flash space left, at least
as of the 20241206 rev8 release.

HP Sure Start
-------------

There is a 16MB flash and a 2MB flash. Read this page for info:
<https://doc.coreboot.org/mainboard/hp/hp_sure_start.html>

The page makes it seem more complicated than necessary, from a user's point
of view. What you really need to do is just erase the 2MB flash IC, and flash
only the first 12MB of the 16MB flash IC. A photo is shown below. Consult
the [SPI flashing guide](../install/spi.md) and act as if you were flashing,
but leave out `-w libreboot.rom` (don't write an image), and instead
use the `--erase` option, with your clip connected to the private flash (2MB
flash IC).

You might want to dump the private flash first, just in case (use `-r priv.rom`
or whatever filename you want to dump to, and take two dumps, ensuring that
the hashes match); one dump for the first erase, and another for the next
erase. If they match, then the erase was likely a success. The private (2MB)
flash is inaccessible from your OS. The
system stores hashes of the IFD, GbE and a copy of IFD/GbE in private flash,
restoring them if they were modified, but erasing the private flash disables
this security mechanism.

Coreboot has its own page about this machine:

<https://doc.coreboot.org/mainboard/hp/elitebook_820_g2.html>

Make sure to read and understand all of this first, before attempting
the Libreboot installation, because it's also important when updating
Libreboot later on.

Installation of Libreboot
-------------------------

Make sure to set the MAC address in the flash:
[Modify MAC addresses with nvmutil](../install/nvmutil.md).

Refer to the [Libreboot flashing guides](../install/spi.md)

Here are the flash ICs:

![](https://av.libreboot.org/hp820g2/hp820g2_flash.jpg)

When you flash the 12MB image, please do the following with it:

	dd if=/dev/zero of=4mb.bin bs=4M count=1
	cat libreboot.rom 4mb.bin > libreboot16.rom

Be careful: do not fully flash `libreboot16.rom`

Flash it like this, instead:

```
flashprog -p PROGRAMMER --ifd -i gbe -w libreboot16.rom --noverify-all
flashprog -p PROGRAMMER --ifd -i bios -w libreboot16.rom --noverify-all
flashprog -p PROGRAMMER --ifd -i me -w libreboot16.rom --noverify-all
flashprog -p PROGRAMMER --ifd -i fd -w libreboot16.rom --noverify-all
```

Replace `PROGRAMMER` according to whichever flasher you're using. You could
also replace it with `internal`, if later flashing internally to update an
existing Libreboot installation.

If you're flashing internally, add `--noverify-all` to the flashprog
command.

To erase the 2MB flash, do this:

```
flashprog -p PROGRAMMER --erase
```

Refer generally to the [main flashing guide](../install/) and to
the [external flashing guide](../install/spi.md) so that you can learn how
to actually flash it.

TPM 2.0 potentially supported
-----------------------------

The onboard TPM is an SLB 9660, which supports TPM 1.2 but it is known to be
compatible with TPM 2.0 via firmware upgrade. Although not yet tested, we have
some notes about that here:

[../../tasks/#hp-820-g2-tpm](../../tasks/#hp-820-g2-tpm)

Not yet used meaningfully by Libreboot itself, but the TPM can be used to
implement things like measured boot.

References
----------

See: <https://doc.coreboot.org/soc/intel/broadwell/blobs.html>

Libreboot's build system automatically pulls down the MRC/refcode files, and
modifies the refcode to enable the onboard Intel Gigabit Ethernet (GbE). You
don't need to mess with this at all, when you build Libreboot yourself.

You can see how this works, by looking at the patch which added 820 G2 support:
<https://browse.libreboot.org/lbmk.git/commit/?id=401c0882aaec059eab62b5ce467d3efbc1472d1f>

![](https://av.libreboot.org/hp820g2/hp820g2.jpg)

Yay. If you see this boot screen, you should be proud. This is a really
hard machine to flash.
