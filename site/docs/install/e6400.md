---
title: Flashing the Dell Latitude E6400
x-toc-enable: true
...

Introduction
============

Initial flashing instructions for the E6400.

**ROM images are available in the [Libreboot 20230423
release](../../news/libreboot20230423.md), and subsequent releases.**

**Variants with Nvidia GPUs are NOT supported in Libreboot 20230423
or 20230625.**

**Variants with Nvidia GPUs are supported in Libreboot 20231021 or higher.**

This guide is for those who want libreboot on their Latitude E6400 while
they still have the original Dell BIOS present. This guide can also be
followed (adapted) if you brick your E6400, and you want to recover it.

Variants (nvidia or intel graphics)
========

Dell E6400, E6400 XFR and E6400 ATG are all believed to work. The flashing
instructions are identical, on all of them.

100% Free Software possible (Intel GPU)
=========================

This board can boot entirely *free software* in the flash. The hardware is similar
to that of ThinkPad X200, T400 etc where no-ME setup is possible.

No-microcode setup feasible
----------------------------

The
[microcode bugfixes/mitigations added for GM45](../../news/gm45microcode.md)
are also applicable to this board, for users who are interested. Read that
article for more information.

Libreboot still recommends that boot with CPU microcode updates, by default,
for all the reasons described by Libreboot's [Binary Blobs Reductions
Policy](../../news/policy.md) but this board run reasonably well without them.

A note about GPUs
-----------------

We *confirm that* the Nvidia models are PM45, and therefore will require a VGA
ROM for initialisation. This is supported in Libreboot *after* the 20230625
release, if you compile from source; the `e6400_4mb` target can work on both
variants, but will need the Nvidia VGA ROM inserted to work on Nvidia models.
This insertion is handled automatically in newer lbmk revisions, during build
time, or you can [insert it on a release rom
after 20230625](ivy_has_common.md). - **A Video BIOS Option
ROM is used, in this configuration. Libreboot's
build system automatically downloads this at build time, or it can handle that
for you in the same way if it was scrubbed from a release ROM.**

Models with Intel graphics are GM45, and fully supported in Libreboot
with native initialisation; ROM images are available since Libreboot 20230423.
**The Intel video initialisation is libre, implemented with publicly available
source code via libgfxinit, from the coreboot project.**

Flash chip size {#flashchips}
===============

Use this to find out:

	flashprog -p internal

We believe most/all are 4MB (32Mb) flash sizes, but larger ROM images are
provided for people who wish to upgrade.

MAC address {#macaddress}
===========

The MAC address is part of the ROM image that you're flashing. You can change
it at any time, before or after you've flashed Libreboot; you can also change
it in the *Dell* BIOS, if you really want to. This is for the onboard gigabit
ethernet device.

Refer to [mac\_address.md](../hardware/mac_address.md).

It is recommended that you run *nvmutil*. See:

[nvmutil usage manual](nvmutil.md)

The `nvmutil` software is specifically designed for changing MAC addresses,
and it implements a few more safeguards (e.g. prevents multicast/all-zero
MAC addresses) and features (MAC address randomisation, ability to correct or
intententionally corrupt(disable) GbE sections if you wish, swap GbE parts,
etc). You can *also* run ich9gen, if you wish:

[ich9gen usage manual](ich9utils.md)

Intel GPU: libre video initialisation available
===============================================

Libreboot uses coreboot's native `libgfxinit` on this platform, for
variants with Intel graphics.

For Intel GPU variants, Libreboot 20230423 and up have full support. Simply
flash a release ROM, if you wish.

Nvidia GPU: Video BIOS Option ROM required
==========================================

**NOTE: `nouveau` (Linux video driver) is unstable when it was last tested, in
this setup. Either specify `nomodeset` kernel option, or use another
operating system such as OpenBSD. More information is written on the
[E6400 hardware page](../hardware/e6400.md), regarding OS compatibility.**

This is *unavailable* in Libreboot 20230423 and 20230625, but a future release
will contain support for these variants; for now, you must compile Libreboot
from Git. It is available in the default `master` branch of lbmk, under
build target `e6400_4mb` - either build from source and it gets inserted
automatically, or you can [insert it
manually](ivy_has_common.md) on future release ROMs past Libreboot 20230625.

An earlier experimental revision existed in an experimental branch of
lbmk, as build target `e6400nvidia_4mb`, but it was decided that since SeaBIOS
is the only payload anyway on this board, having just a single build target is
more efficient if that can (and it does) support both variants. It was therefore
added to the master branch.

Actual installation is the same as with regular E6400 (Intel GPU) variants.
Refer to the [E6400 flashing instructions](../docs/install/e6400.md).

Problems with Linux video drivers on Nvidia
-------------------------------------------

Technically, there is nothing wrong with Libreboot itself, but the `nouveau`
driver hangs/crashes on Nvidia models, after booting Linux with the Nvidia VGA
ROM loaded from coreboot.

Until that is fixed, you must specify `nomodeset` in your Linux kernel boot
parameters.

Refer to [development
discussion](https://codeberg.org/libreboot/lbmk/issues/14#issuecomment-907758)
for more information - [testers needed!](../maintain/testing.md)

The *Intel* GPU variant of E6400 is more stable, and works fully, with full
acceleration - the Nvidia models can only be run in software, and the BSD
systems only have the slow `nv` driver (which is nonetheless stable).

Nvidia errata
-------------

**BEFORE** you flash it, please know that support for Nvidia variants is
a **proof of concept**. Known issues exist. For more information, please
read the [E6400 info page](../hardware/e6400.md), [E6400 nvidia news
page](../../news/e6400nvidia.md) and the [development discussion via
codeberg](https://codeberg.org/libreboot/lbmk/issues/14#issuecomment-907758).

How to flash internally (no diassembly)
=======================================

Warning for BSD users
---------------------

**NOTE (15 October 2023): The util is now called `dell-flash-unlock`, but it
was previously called `e6400-flash-unlock`. Links have been updated.**

BSD *boots* and works properly on these machines, but take note:

Nicholas's [dell-flash-unlock](https://browse.libreboot.org/lbmk.git/plain/util/dell-flash-unlock/dell_flash_unlock.c)
utility has been ported to OpenBSD, but *other* BSDs are assumed unsupported for
now.

NOTE: Libreboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 27 January 2024, which is a fork of flashrom.

NOTE: BSD is mentioned above, but the only BSD tested for `dell-flash-unlock`
is OpenBSD, as of 15 October 2023.

Flashing from Linux
-------------------

MAKE SURE you boot with this Linux kernel parameter: `iomem=relaxed` - this
disables memory protections, permitting `/dev/mem` access needed by flashprog.
The flash is memory mapped and flashprog accesses it via `/dev/mem`.

You can flash Libreboot directly from the vendor (Dell) BIOS, without taking
the machine apart. It can be done entirely from Linux. It will probably also
work on BSD systems, but it has only been testing on Linux thus far.

**NOTE (15 October 2023): The util is now called `dell-flash-unlock`, but it
was previously called `e6400-flash-unlock`. Links have been updated.**

Check `util/dell-flash-unlock` in the `lbmk.git` repository, or in release
archives for Libreboot releases from 20230423 onwards.

Go in there:

	cd util/dell-flash-unlock
	make

With this program, you can unlock the flash in such a way where everything
is writeable. Information about how to use it is in the `README.md` file which
is included in that program's directory, or you can read it online here:

<https://browse.libreboot.org/lbmk.git/plain/util/dell-flash-unlock/README.md>

Literally just run that program, and do what it says. You run it once, and shut
down, and when you do, the system brings itself back up automatically.  Then
you run it and flash it unlocked. Then you run it again. The source code is
intuitive enough that you can easily get the gist of it; it's writing some EC
commands and changing some chipset config bits. The EC on this machine is
hooked up to the `GPIO33` signal, sometimes called `HDA_DOCK_EN`, which sets
the flash descriptor override thus disabling any flash protection by the IFD.
It also bypasses the SMM BIOS lock protection by disabling SMIs, and Dell's
BIOS doesn't set any other type of protection either such as writing to
Protected Range registers.

When you flash it, you can use this command:

	flashprog -p internal -w libreboot.rom

Where `libreboot.rom` is your E6400 ROM. *Make sure* it's the right one.
If flashprog complains about multiple flash chips detected, just pick one of
them (doesn't matter which one). On *most* Dell machines, the most correct
would probably be this option in flashprog: `-c MX25L3205D/MX25L3208D`.

So:

	flashprog -p internal -w libreboot.rom -c MX25L3205D/MX25L3208D

When you see flashprog say `VERIFIED` at the end, that means the flash was
successful. If you don't see that, or you're unsure, please [contact the
Libreboot project via IRC](../../contact.md).

BACK UP THE FACTORY BIOS
========================

The `-w` option flashes `libreboot.rom`. You may consider *backing up* the
original Dell BIOS first, using the -r option:

	flashprog -p internal -r backup.rom -c MX25L3205D/MX25L3208D

Do this while in a flashable state, after the 2nd run of `dell-flash-unlock`.

Make sure the `backup.rom` file gets backed up to an external storage media,
not the E6400 itself.

With this method, you can probably flash it within 5 minutes. Again, zero
disassembly required!

How to flash externally
=========================

Refer to [spi.md](spi.md) as a guide for external re-flashing.

The SPI flash chip shares a voltage rail with the ICH9 southbridge, which is
not isolated using a diode. As a result, powering the flash chip externally
causes the ICH9 to partially power up and attempt to drive the SPI clock pin
low, which can interfere with programmers such as the Raspberry Pi. See
[RPi Drive Strength](spi.md#rpi-drive-strength) for a workaround.

Have a look online for videos showing how to disassemble, if you wish to
externally re-flash.

