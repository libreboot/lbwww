---
title: Dell Latitude E6400
x-toc-enable: true
...

<div class="specs">
<center>
<img tabindex=1 alt="Dell Latitude E6400" class="p" src="https://av.libreboot.org/e6400/e6400-seabios.jpg" /><span class="f"><img src="https://av.libreboot.org/e6400/e6400-seabios.jpg" /></span> <img tabindex=1 alt="Dell Latitude E6400 XFR" class="p" style="max-width:24em" src="https://av.libreboot.org/e6400/e6400xfr-seabios.jpg" /><span class="f"><img src="https://av.libreboot.org/e6400/e6400xfr-seabios.jpg" /></span>
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | Dell                                           |
| **Name**                   | Latitude E6400                                 |
| **Variants**               | E6400, E6400 XFR and E6400 ATG are supported   |
| **Released**               | 2009                                           |
| **Chipset**                | Intel Cantiga GM45(Intel GPU)/PM45(Nvidia GPU) |
| **CPU**                    | Intel Core 2 Duo (Penryn family).              |
| **Graphics**               | Intel GMA 4500MHD (and NVidia Quadro NVS 160M 
                                                         on some models) |
| **Display**                | 1280x800/1440x900 TFT                          |
| **Memory**                 | 2 or 4GB (Upgradable to 8GB)                   |
| **Architecture**           | x86_64                                         |
| **EC**                     | SMSC MEC5035 with proprietary firmware         |
| **Original boot firmware** | Dell BIOS                                      |
| **Intel ME/AMD PSP**       | Present. Can be completely disabled.           |
| **Flash chip**             | SOIC-8 4MiB or 2MiB+4MiB                       |


```
W+: Works without blobs; 
N: Doesn't work; 
W*: Works with blobs; 
U: Untested; 
P+: Partially works; 
P*: Partially works with blobs
```

| ***Features***                                    |    |
|---------------------------------------------------|----|
| **Internal flashing with original boot firmware** | W+ |
| **Display (if Intel GPU)**                        | W+ |
| **Display (if Nvidia GPU)**                       | W* |
| **Audio**                                         | W+ |
| **RAM Init**                                      | W+ |
| **External output**                               | W+ |
| **Display brightness**                            | P+ | 

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**                  | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB**     | Works     |
</div>
Introduction
============

Known supported variants: E6400, E6400 XFR and E6400 ATG. This page has
been updated to include information about Nvidia GPU variants. See news post:
[Dell Latitude E6400 XFR support confirmed, plus experimental Nvidia GPU
support on E6400 variants](../../news/e6400nvidia.md).

**To install Libreboot, see: [E6400 installation
instructions](../install/e6400.md)**

ROM images for Dell Latitude E6400 are available for flashing in the Libreboot
release 20230423 onwards, or you can compile a ROM image for installation via
lbmk, see: [build instructions](../build/)

There are two possible flash chip sizes for the E6400: 4MiB (32Mbit) or 2+4MiB
(16Mbit+32MBit). Libreboot presently supports the 4MiB version, and provides
8MiB images for those who upgrade their flash to 8MiB or 16MiB. There appears
to be several possible mainboard PCBs for the E6400, which we believe mostly
affects the GPU configuration and the number of available SPI flash footprints:

- LA-3801P: iGPU, possibly dual SPI (however only one may be populated)
- LA-3803P: dGPU, dual SPI (however only one may be populated)
- LA-3805P: iGPU, single SPI flash (4MiB)
- LA-3806P: dGPU, unknown SPI configuration (likely at least 4MiB)

These PCB numbers can be found either under the black plastic in the RAM slots
on the bottom (CPU side) of the board, the top left corner near the VGA port
(top side, under the keyboard and palmrest), or near the CPU backplate (only
requires removal of the keyboard).

We believe that all boards will have at least a single 4MiB flash chip,
regardless of the number of SPI footprints. This is likely the most common
configuration on most available systems. The 2+4MiB configuration likely
would have only been used on systems with full Intel ME firmware with AMT
functionality, though this configuration has not yet been encountered.

Most people will want to use the 4MiB images.

Intel GPU: 100% Free Software is possible
---------------

This is a GM45/PM45 platform, so completely libre initialisation in
coreboot is possible, provided by default in Libreboot.

Management Engine (ME) firmware removed
-------------------------

This port in Libreboot makes use of `ich9gen` from ich9utils, which
you can read about in the [ich9utils manual](../install/ich9utils.md) - this
creates a no-ME setup. The Intel Management Engine firmware (ME) is completely
removed, and the ME disabled, just like on ThinkPad X200, T400 and so on.

*The E6400 laptops may come with the ME (and sometimes AMT in addition) before
flashing libreboot. Dell also sold configurations with the ME completely
disabled, identifiable by a yellow sticker reading "3 ME Disabled" inside the
bottom panel. This config sets the MeDisable bit in the IFD and sets the ME
region almost entirely to 1's, with the occasional 32-bit value (likely not
executable). libreboot disables and removes it by using a modified descriptor:
see [../install/ich9utils.md](../install/ich9utils.md)*
(contains notes, plus instructions)

Issues pertaining to Nvidia GPU variants
========================================

Copper shim for GPU cooling
---------------------------

NOTE: this section does *not* apply to XFR or ATG variants of E6400, which have
a much beefier heatsink by default.

The *default* heatsink in Nvidia variants of E6400 (regular model) has thermal
paste for the CPU, and a thermal *pad* for the GPU. This pad is woefully
inadequate, but replacing it with *paste* is a bad idea, because of the gap
there would be between heatsink plate and GPU die.

A solution for this would be to use a *copper shim*, with paste on each side,
to replace the thermal pad.

This eBay seller seems to make and sell a lot of copper shims, specifically
for E6400:

**SELLER LINK REMOVED.** - one will not be re-added. Putting ebay links on the
Libreboot site is folly, because they disappear. Just search for it and see if
you can find one for purchase. It's literally just a small bit of copper cut
smooth to just the right size. Actually, there's a lot of engineering behind
that, but installation is very simple, and any decent seller will provide
guidance.

If you buy one of those, could you measure it? Tell Libreboot the dimensions.
Get in touch with us. It would be nice to know precise specs, but that seller
provides what you need. If you find similar listings elsewhere, please also
let us know.

The shim will greatly reduce GPU temperatures, and probably improve performance
due to less GPU throttling as a result of heat.

Nouveau(in Linux) currently broken
----------------------------------

Nouveau is the libre driver in Linux, for Nvidia graphics. Nvidia themselves
do not provide binary drivers anymore, for these GPUs. It crashes in Linux,
when you try to start Xorg (Wayland is untested).

If you're booting an Nvidia variant in Linux, boot Linux with
the `nomodeset` kernel option at boot time. This means that graphics are
rendered in software.

Development discussion, for Nvidia variants of E6400, is available here:

<https://codeberg.org/libreboot/lbmk/issues/14>

OpenBSD's Nvidia driver works perfectly
---------------------------------------

OpenBSD 7.3 was tested, on my Nvidia-model E6400, and Xorg works OK with
the `nv` driver.

<img tabindex=1 class="l" style="max-width:35%" src="https://av.libreboot.org/openbsd.jpg" /><span class="f"><img src="https://av.libreboot.org/openbsd.jpg" /></span>

See: <https://www.openbsd.org/>

OpenBSD is a complete free 4.4BSD Unix operating system focused on portability,
security and *code correctness*. It's quite useable for most day to day tasks.

You can find information in Libreboot about BSD operating systems on the
main guide:

* [BSD Operating Systems](../bsd/)

FreeBSD and newer Linux (e.g. Archlinux) untested!
--------------------------------------------------

FreeBSD has not yet been tested, as far as we know, but it should work.

[Testers needed! Please get in touch!](../maintain/testing.html)

**At the time of writing this post, FreeBSD
and newer Linux have not yet been tested** (I plan to test *Arch Linux*), but
the older Linux/Mesa version in Debian 11.6 works just fine in the Dell BIOS,
and I've confirmed that it uses the exact same Video BIOS Option ROM.

Desktop environment / window manager on OpenBSD + Performance notes
-------------------------------------------------------------------

TODO: This section could probably be moved to its own section. It's not really
relevant to Libreboot per se, but it may help a few people.

Again, Linux's nouveau driver is currently broken. I've been playing with my
E6400 (nvidia model) for a while and I've found that these things are a *must*
for performance (the machine otherwise lags, openbsd's `nv` driver isn't quite
as good as nouveau, when the nouveau one works that is):

* Use a lightweight desktop environment like LXQt, or lightweight window
  manager (OpenBSD has `cwm` in base, and it's excellent)
* Install `obsdfreqd` which scales down the CPU speed during idle state; the
  GPU has a poor thermal pad for cooling and so if the CPU is running hot,
  that doesn't bode well for GPU temperatures either, and the GPU is likely
  lagging due to heat:

How to install `obsdfreqd`:

	pkg_add obsdfreqd
	rcctl enable obsdfreqd

Now, before you start it, make sure `apmd` is disabled; it can be used, but
not with the `-A` flag:

	rcctl stop apmd
	rcctl disable apmd

Now start obsdfreqd:

	rcctl start obsdfreqd

You will be well served to perform the copper shim mod, for GPU cooling.
With `obsdfreqd`, your laptop will run much cooler. This is generally a good
idea anyway, especially on laptops, to save electricity.

Of course, there are many tweaks that you can do to OpenBSD but the key is:
don't use heavy bloated software. The term *lightweight* is misleading anyway;
if the software does its job efficiently, and you're happy with it, then it is
by definition superior for your purposes. So, "lightweight" is simply a word
for "efficient" in many contexts. We should encourage the use and development
of highly efficient software that runs more smoothly on old machines. The
elitist attitude of *just buy a new computer* is quite damaging; re-use is
always better, when that is feasible and safe. The power of BSD (and Linux) is
precisely that you can tweak it to get the most use out of older hardware..

Another nice hint: higher resolution video like 1080p 60fps or above won't
play smoothly at all in a web browser. In testing at least on OpenBSD 7.3,
Firefox seems to have the best performance among all the web browsers, at least
when I used it. Anything 720p 30/60fps will work ~OK.

For YouTube, you could use yt-dlp, which is available in ports, and use mpv to
stream via yt-dlp. Or download manually with yt-dlp and play offline. See:

<https://github.com/yt-dlp/yt-dlp>

<https://mpv.io/>

Another hint: for watching youtube in the browser, Invidious works quite well.
It's a frontend that lets you view it by proxy, and there are many instances
of it online. For a list of instances, see:

<https://redirect.invidious.io/>

Unlike youtube.com, watching youtube via invidious works even with JavaScript
turned off in the browser. You can use it to also search YouTube, and then
paste the youtube.com link into yt-dlp or mpv; Invidious websites themselves
also often provide a download button for videos.

The yt-dlp software may also work on a few other websites besides YouTube.
Running with JavaScript turned *off* is generally recommended for performance,
especially on slower machines, turning it on only when you need it. Many
websites are just full of junk nowadays.

