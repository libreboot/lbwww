---
title: ThinkPad T400 
x-toc-enable: true
...

<div class="specs">
<center>
<img tabindex=1 alt="ThinkPad T400" class="p" src="https://av.libreboot.org/t400/boot1.jpg" /><span class="f"><img src="https://av.libreboot.org/t400/boot1.jpg" /></span>
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | Lenovo                                         |
| **Name**                   | ThinkPad T400                                  |
| **Released**               | 2009                                           |
| **Chipset**                | Intel Cantiga GM45                             |
| **CPU**                    | Intel Core 2 Duo (Penryn family). A Quad-core 
                        mod exists, replacing the Core 2 Duo with a Core Quad |
| **Graphics**               | Intel GMA 4500MHD (and ATI Mobility Radeon HD 
                                                         3650 on some models) |
| **Display**                | 1280x800/1440x900 TFT                          |
| **Memory**                 | 2 or 4GB (Upgradable to 8GB)                   |
| **Architecture**           | x86_64                                         |
| **EC**                     | Proprietary                                    |
| **Original boot firmware** | LenovoBIOS                                     |
| **Intel ME/AMD PSP**       | Present. Can be completly disabled.            |
| **Flash chip**             | SOIC-8/SOIC-16/WSON-8 4MiB/8MiB (Upgradable 
                                                                    to 16MiB) |
```
W+: Works without blobs; 
N: Doesn't work; 
W*: Works with blobs; 
U: Untested; 
P+: Partially works; 
P*: Partially works with blobs
```

| ***Features*** |                                       |
|----------------|---------------------------------------|
| **Internal flashing with original boot firmware** | N  |
| **Display**                                       | W+ |
| **Audio**                                         | W+ |
| **RAM Init**                                      | W+ |
| **External output**                               | W+ |
| **Display brightness**                            | P+ | 

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**              | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB** | Works     |
</div>
Dell Latitude E6400
===================

**If you haven't bought an T400 yet: the [Dell Latitude
E6400](../../news/e6400.md) is much easier to flash; no disassembly required,
it can be flashed entirely in software from Dell BIOS to Libreboot. It is the
same hardware generation (GM45), with same CPUs, video processor, etc.**

Introduction
============

It is believed that all or most laptops of the model T400 are compatible. See notes
about [CPU
compatibility](../install/t400_external.html#cpu_compatibility) for
potential incompatibilities.

There are two possible flash chip sizes for the T400: 4MiB (32Mbit) or
8MiB (64Mbit). This can be identified by the type of flash chip below
the palmrest: 4MiB is SOIC-8, 8MiB is SOIC-16.

*The T400 laptops come with the ME (and sometimes AMT in addition)
before flashing libreboot. libreboot disables and removes it by using a
modified descriptor: see [../install/ich9utils.md](../install/ich9utils.md)*
(contains notes, plus instructions)

Flashing instructions can be found at
[../install/\#flashprog](../install/#flashprog)

NOTE: Libreboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 27 January 2024, which is a fork of flashrom.

EC update {#ecupdate}
=========

It is recommended that you update to the latest EC firmware version. The
[EC firmware](../../faq.md#ec-embedded-controller-firmware) is separate from
libreboot, so we don't actually provide that, but if you still have
Lenovo BIOS then you can just run the Lenovo BIOS update utility, which
will update both the BIOS and EC version. See:

-   [../install/#flashprog](../install/#flashprog)
-   <http://www.thinkwiki.org/wiki/BIOS_update_without_optical_disk>

NOTE: this can only be done when you are using Lenovo BIOS. How to
update the EC firmware while running libreboot is unknown. libreboot
only replaces the BIOS firmware, not EC.

Updated EC firmware has several advantages e.g. bettery battery
handling.

The T400 is almost identical to the X200, code-wise. See
[x200.md](x200.md).
