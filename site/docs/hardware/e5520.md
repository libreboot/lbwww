---
title: Dell Latitude E5520
x-toc-enable: true
...

<div class="specs">
<center>
Dell Latitude E5520
</center>

| ***Specifications***       |                                                |
|----------------------------|------------------------------------------------|
| **Manufacturer**           | Dell                                           |
| **Name**                   | Latitude E5520                                 |
| **Variants**               | E5520 with Intel GPU supported                 |
| **Released**               | 2012                                           |
| **Chipset**                | Intel Sandy Bridge                             |
| **CPU**                    | Intel Core i3, i5 or i7                        |
| **Graphics**               | Intel HD 4000 and unsupported Nvidia NVS 5200M |
| **Display**                | 1366x768/1600x900 TFT                          |
| **Memory**                 | 4 or 8GB (Upgradable to 16GB)                  |
| **Architecture**           | x86_64                                         |
| **EC**                     | SMSC MEC5055 with proprietary firmware         |
| **Original boot firmware** | Dell UEFI                                      |
| **Intel ME/AMD PSP**       | Present, neutered                              |
| **Flash chip**             | 2xSOIC-8, 6MiB (4MiB and 2MiB in combination) |


```
W+: Works without blobs;
N: Doesn't work;
W*: Works with blobs;
U: Untested;
P+: Partially works;
P*: Partially works with blobs
```

| ***Features***                                    |    |
|---------------------------------------------------|----|
| **Internal flashing with original boot firmware** | W+ |
| **Display (if Intel GPU)**                        | W+ |
| **Display (if Nvidia GPU)**                       | U  |
| **Audio**                                         | W+ |
| **RAM Init**                                      | W+ |
| **External output**                               | W+ |
| **Display brightness**                            | P+ |

| ***Payloads supported***  |           |
|---------------------------|-----------|
| **GRUB**                  | Works     |
| **SeaBIOS**               | Works     |
| **SeaBIOS with GRUB**     | Works     |
</div>
Introduction
============

**Libreboot 20231021 and releases newer than this have ROMs available for
Dell Latitude E5520.**

ROM images for Dell Latitude E5520 are available for flashing in the Libreboot
releases *after* 20230625, or you can compile a ROM image for installation via
lbmk, see: [build instructions](../build/)

Only the Intel GPU variants are supported, currently. All models with Intel GPU
are assumed to work.

Unlike the E6400, this one does require a neutered Intel ME image to run. This
means running it through `me_cleaner` before flashing; the Libreboot build
system does this automatically, during build, or you can insert a neutered
ROM image using the vendor scripts, see guide:

[Insert vendor files](../install/ivy_has_common.md)

As with the E6400, this one is flashable in software, from Dell UEFI firmware
to Libreboot. Please refer to the installation instructions.

**To install Libreboot, see: [E5520 installation
instructions](../install/e6430.md)**
