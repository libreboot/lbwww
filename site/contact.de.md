---
title: Kontakt
x-toc-enable: true
...

Buy Libreboot pre-installed
==========

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

If you're installing Libreboot yourself, support for that is also available.
Contact information (IRC, mailing list etc) is below:

User support
============

IRC oder Reddit werden bevorzugt, sofern Du eine Support Anfrage hast (IRC empfohlen).
Für Informationen bzgl. IRC and Reddit siehe unten.

Mailing list
============

Libreboot has this mailing list:
<https://lists.sr.ht/~libreboot/libreboot>

The email address is [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Entwicklungs Diskussion
======================

Siehe unter
[der Git Seite](git.md) für Informationen wie Du dich an der Entwicklung beteiligen kannst.

Hier finden sich ebenso Anleitungen zum Senden von Patches (via Pull-Requests).

IRC Chatraum
============

IRC ist hauptsächlich der Weg um Kontakt Libreboot Projekt aufzunehmen. `#libreboot` auf Libera
IRC.

Webchat:
<https://web.libera.chat/#libreboot>

Libera ist eines der grössten IRC Netzwerke, welches für Libre Software Projekte verwendet wird.
Mehr Infos gibt es hier: <https://libera.chat/>

Wenn Du dich mit deinem bevorzugten IRC Klienten verbinden möchtest (z.B. weechat or irssi),
anbei die Verbindungsdetails:

* Server: `irc.libera.chat`
* Channel: `#libreboot`
* Port (TLS): `6697`
* Port (non-TLS): `6667`

Wir empfehlen, dass Du Port `6697` mit aktivierter TLS Verschlüsselung verwendest.  

Es wird empfohlen SASL für die Authentifizierung zu verwenden. Diese Seiten auf der Libera
Website erläutern wie dies funktioniert:

* WeeChat SASL Anleitung: <https://libera.chat/guides/weechat>
* Irssi SASL Anleitung: <https://libera.chat/guides/irssi>
* HexChat SASL Anleitung: <https://libera.chat/guides/hexchat>

Grundsätzlich solltest Du die Dokumentation der von Dir verwendeten IRC Software konsultieren.

Soziale Medien
============

Libreboot existiert offiziell an vielen Orten.

Mastodon
--------

Gründerin und Haupt-Entwicklerin, Leah Rowe, ist auf Mastodon:

* <https://mas.to/@libreleah>

Leah kann zudem unter dieser eMail kontaktiert werden:
[leah@libreboot.org](mailto:leah@libreboot.org)

Reddit
------

Hauptsächlich verwendet als Support Kanal und für Veröffentlichung von Neuigkeiten: 
<https://www.reddit.com/r/libreboot/>
