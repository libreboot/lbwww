---
title: Contact the Libreboot project
x-toc-enable: true
...

Buy Libreboot pre-installed
--------------------------

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

If you're installing Libreboot yourself, support for that is also available.
Contact information (IRC, mailing list etc) is below:

User support
-------------

IRC or Reddit are recommended, if you wish to ask for support (IRC recommended).
See below for information about IRC and Reddit.

Mailing list
------------

Libreboot has this mailing list:
<https://lists.sr.ht/~libreboot/libreboot>

The email address is [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Development discussion
--------------------

See notes
on [the Git page](git.md) for information about how to assist with development.

Instructions are also on that page for sending patches (via pull requests).

IRC chatroom
-------------

IRC is the main way to contact the libreboot project. `#libreboot` on Libera
IRC.

Webchat:
<https://web.libera.chat/#libreboot>

Libera is one of the largest IRC networks, used for Libre Software projects.
Find more about them here: <https://libera.chat/>

If you wish to connect using your preferred client (such as weechat or irssi),
the connection info is as follows:

* Server: `irc.libera.chat`
* Channel: `#libreboot`
* Port (TLS): `6697`
* Port (non-TLS): `6667`

We recommend that you use port `6697` with TLS encryption enabled.  

It is recommend that you use SASL for authentication. These pages on the Libera
website tells you how:

* WeeChat SASL guide: <https://libera.chat/guides/weechat>
* Irssi SASL guide: <https://libera.chat/guides/irssi>
* HexChat SASL guide: <https://libera.chat/guides/hexchat>

In general, you should check the documentation provided by your IRC software.

Social media
-------------

libreboot exists officially on many places.

### Mastodon

The founder and lead developer, Leah Rowe, is on Mastodon:

* <https://mas.to/@libreleah>

Leah can also be contacted by this email address:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Mostly used as a support channel, and also for news announcements:
<https://www.reddit.com/r/libreboot/>
