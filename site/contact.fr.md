---
title: Contact the Libreboot project
x-toc-enable: true
...

Où acheter du matériel avec Libreboot pré-installé
---------------------------------------------

Si vous souhaitez une installation professionnelle,  Minifree Ltd vend des ordinateurs avec [Libreboot pré-installé](https://minifree.org/). L'entreprise propose également [un service d'installation de Libreboot](https://minifree.org/product/installation-service/) si vous voulez l'installer sur votre propre machine.

Leah Rowe, fondateur et principal développeur du projet Libreboot, est également propriétaire de Minifree Ltd. Les ventes contribuent à financer le développement du projet Libreboot.

Si vous installez Libreboot vous-même, un support est disponible via différents canaux de communication (IRC, mail, etc.) :


Suport utilisateur
------------------

IRC et Reddit sont recommandés pour demander de l'aide (IRC est à privilégier) :


Mail
----

Libreboot dispose d'une liste de diffusion : <https://lists.sr.ht/~libreboot/libreboot>

L'adresse mail est [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Discussion sur le developpement
-------------------------------

Pour participer au développement, consultez la pag [GIT](git.md)  du projet. Vous y trouverez des instructions détaillées sur l'envoi de patchs via pull request.

IRC chatroom
------------

Le chat IRC est le principal moyen de contact pour le projet Libreboot via `#libreboot` sur Libera IRC.

Webchat:
<https://web.libera.chat/#libreboot>

Libera est un des plus grands réseaux IRC utilisé pour les projets en lien avec le logiciel libre. Vous trouverez plus d'information ici : <https://libera.chat/>

Si vous souhaitez vous connecter via votre client préféré ( comme weechat ou irssi), veuillez utiliser ces informations :

* Server: `irc.libera.chat`
* Channel: `#libreboot`
* Port (TLS): `6697`
* Port (non-TLS): `6667`

Nous vous recommandons d'utiliser le port `6697` avec l'encryption TLS activée.

Il est recommandé d'utiliser SASL pour l'authentification. Ces pages vous indiqueront comment l'utiliser :

* WeeChat SASL guide: <https://libera.chat/guides/weechat>
* Irssi SASL guide: <https://libera.chat/guides/irssi>
* HexChat SASL guide: <https://libera.chat/guides/hexchat>

De manière générale, reportez-vous à la documentation de votre logiciel IRC.


Réseaux sociaux
---------------

Libreboot existe officiellement sur différents réseaux sociaux.

### Mastodon

Le fondateur et principal developpeur, Leah Rowe, est sur Mastodon:

* <https://mas.to/@libreleah>

Leah peut également être contacté par mail :
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Généralement utilisé pour le support mais aussi pour l'annonces des dernières nouveautés :
<https://www.reddit.com/r/libreboot/>
