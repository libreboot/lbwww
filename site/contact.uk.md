---
title: Зв'язок
x-toc-enable: true
...

Buy Libreboot pre-installed
---------------------------

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

If you're installing Libreboot yourself, support for that is also available.
Contact information (IRC, mailing list etc) is below:

Підтримка користувачів
----------------------

IRC або Reddit рекомендовані, якщо ви бажаєте попросити про допомогу (найкраще IRC).
Дивіться інформацію нижче щодо IRC та Reddit.

Mailing list
------------

Libreboot has this mailing list:
<https://lists.sr.ht/~libreboot/libreboot>

The email address is [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Обговорення розробки
--------------------

Зараз, подивіться нотатки
на [сторінці Git](git.md) для інформації щодо допомоги з розробкою.

На цій сторінці також знаходяться інструкції по відправці патчів (через pull request'и).

Кімната IRC
-----------

IRC це головний спосіб зв'язку з проектом Libreboot. `#libreboot` на Libera
IRC.

Веб-версія:
<https://web.libera.chat/#libreboot>

Libera є однією з найбільших мереж IRC, використовуємих для проектів вільного програмного
забезпечення. Знайти про них більше можна тут: <https://libera.chat/>

Якщо ви бажаєте під'єднатися за допомогою вашого улюбленного клієнта (такого як weechat або irssi),
інформація для під'єднання наступна:

* Сервер: `irc.libera.chat`
* Канал: `#libreboot`
* Порт (TLS): `6697`
* Порт (не TLS): `6667`

Ми радимо вам використовувати порт `6697` з увімкненим TLS шифруванням.  

Рекомендовано використовувати SASL для аутентифікації. Ці сторінки на веб-сайті Libera
пояснять вам як:

* Керівництво WeeChat SASL: <https://libera.chat/guides/weechat>
* Керівництво Irssi SASL: <https://libera.chat/guides/irssi>
* Керівництво HexChat SASL: <https://libera.chat/guides/hexchat>

Взагалі, вам варто перевірити документацію, яка передбачена вашою програмою IRC.

Соціальні мережі
----------------

Libreboot офіційно існує в багатьох місцях.

### Mastodon

Засновник та головний розробник, Лія Роу, є в Mastodon:

* <https://mas.to/@libreleah>

Також можливо зв'язатися з Лією за ії електронною адресою:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Найбільше використовується як канал підтримки, та також для оголошення новин:
<https://www.reddit.com/r/libreboot/>
