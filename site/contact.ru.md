---
title: Контакты
x-toc-enable: true
...

Купите Libreboot предустановленным
----------------------------------

Если вы хотите, чтобы профессионал установил Libreboot для вас, то Minifree Ltd продает [Libreboot предустановленным](https://minifree.org/) на определенном оборудование, и также предоставляет сервис по [установке Libreboot](https://minifree.org/product/installation-service/) на ваши машины. 

Основатель и ведущий разработчик Libreboot, Лия Роу, владеет и управляет Minifree; продажи обеспечивают финансирование для Libreboot.

Если вы самостоятельно устанавливаете Libreboot, вы можете получить поддержку:

Поддержка пользователей
-----------------------

IRC и Reddit предпочительнее, если вы хотите попросить помощи (IRC рекомендуется). Информация об IRC и Reddit ниже.

Почтовая рассылка
-----------------

У Libreboot есть своя почтовая рассылка: <https://lists.sr.ht/~libreboot/libreboot>

Адрес электронной почты: [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Обсуждение разработки
---------------------

Смотрите [страницу по Git](git.md) для того, чтобы узнать, как помогать с разработкой Libreboot.

На этой странице также содержаться инструкции, как отправлять патчи (с помощью pull request).

Чат в IRC
---------

IRC - главный метод связи с проектом Libreboot. `#libreboot` на Libera IRC.

Чат в вебе:
<https://web.libera.chat/#libreboot>

Libera - самая большая сеть IRC, используемая для свободных проектов. Узнайте больше здесь: <https://libera.chat/>

Если вы хотите подключиться к IRC через ваш любимый клиент (например, weechat или irssi):

* Сервер: `irc.libera.chat`
* Канал: `#libreboot`
* Порт (TLS): `6697`
* Порт (не TLS): `6667`

Мы рекомендуем использовать порт `6697` со включенным TLS шифрованием.

Мы также рекомендуем использовать SASL для аутентификации. Эти страницы на сайте Libera IRC расскажут как это сделать:

* WeeChat SASL guide: <https://libera.chat/guides/weechat>
* Irssi SASL guide: <https://libera.chat/guides/irssi>
* HexChat SASL guide: <https://libera.chat/guides/hexchat>

В общем, вы должны проверить документацию вашего клиента для IRC.

Социальные сети
---------------

### Mastodon

Основатель и велущий разработчик, Лия Роу, есть на Mastodon:

* <https://mas.to/@libreleah>

Связаться с Лией также можно и по этому адресу электронной почты:
[leah@libreboot.org](mailto:leah@libreboot.org)

### Reddit

Чаще всего используется для поддержки пользователей, а также для новостей и анонсов:
<https://www.reddit.com/r/libreboot/>
