---
title: Frequently Asked Questions about Libreboot firmware
x-toc-enable: true
...

**NOTE: Libreboot standardises on [flashprog](https://flashprog.org/wiki/Flashprog)
now, as of 27 January 2024, which is a fork of flashrom.
The reason why was explained, in
the [Libreboot 20240225 release](news/libreboot20240225.md#flashprog-now-used-instead-of-flashrom)**

AKA Frequently Questioned Answers

Disable security before flashing
--------------------------------

Before internal flashing, you must first disable `/dev/mem` protections. Make
sure to re-enable them after you're finished.

See: [Disabling /dev/mem protection](docs/install/devmem.md)

Buy Libreboot pre-installed
---------------------------

If you want professional installation, Minifree Ltd sells [Libreboot
pre-installed](https://minifree.org/) on select hardware, and it also provides
a [Libreboot preinstall service](https://minifree.org/product/installation-service/)
if you want to send your machine in to have Libreboot installed for you.

Leah Rowe, the founder and lead developer of Libreboot, also owns and
operates Minifree Ltd; sales provide funding for the Libreboot project.

Important issues
----------------

### How to compile libreboot from source

Refer to the [lbmk build instructions](docs/build/).

### How does the build system work?

Refer to the [lbmk maintenance manual](docs/maintain/).

### Do not use CH341A!

This SPI flasher will damage your chip, and the motherboard that it is connected
to.

Read the notes about CH341A on [docs/install/spi.md](docs/install/spi.md) to
learn more.

### How Can I Help

You do not need to be a skilled developer in order to help the project
substantially.
If you have a board supported by Coreboot, consider [porting](/docs/maintain/porting.md)
it to Libreboot.

If you have a board supported in Libreboot then please consider becoming a
tester.
Testing involves minimal effort and really helps out the project.
See the [board maintainers documentation](/docs/maintain/testing.md)
if you are interested in testing roms before they are released.

### Uneven backlight on GM45 ThinkPads

We don't know how to detect the correct PWM value to use in
coreboot, so we just use the default one in coreboot which has
this issue on some CCFL panels, but not LED panels.

You can work around this in your distribution, by following the notes at
[docs: backlight control](../docs/misc/#finetune-backlight-control-on-intel-gpus).

### GM45 thinkpad ethernet port doesn't autoconnect

This was observed on some systems using network-manager. This happens
both on the original BIOS and in libreboot. It's a quirk in the
hardware. On debian systems, a workaround is to restart the networking
service when you connect the ethernet cable:

	sudo service network-manager restart

On systemd-based distros, you might try:

	sudo systemctl restart network-manager

(the service name might be different for you, depending on your
configuration)

### PIKE2008 module hangs KGPE-D16 / KCMA-D8

Loading the option ROM from the PIKE2008 module on either ASUS KCMA-D8
or KGPE-D16 causes the system to hang at boot. It's possible to use
this in the payload (if you use a linux kernel payload, like linuxboot),
or to boot (with SeaGRUB and/or SeaBIOS) from regular SATA and then use
it in Linux. The Linux kernel is capable of using the PIKE2008
module without loading the option ROM.

### How to save kernel panic logs on thinkpad laptops?

The easiest method of doing so is by using the kernel's netconsole
and reproducing the panic. Netconsole requires two machines, the one that is
panicky (source) and the one that will receive crash logs (target). The
source has to be connected with an ethernet cable and the target has to be
reachable at the time of the panic. To set this system up, execute the
following commands as root on the source (`source#`) and normal user on
the target (`target$`):

1.  Start a listener server on the target machine (netcat works well):

	`target$ nc -u -l -p 6666`

2.  Mount configfs (only once per boot, you can check if it is already mounted
    with `mount | grep /sys/kernel/config`. This will return no output
    if it is not).

	`source# modprobe configfs`

	`source# mkdir -p /sys/kernel/config`

	`source# mount none -t configfs /sys/kernel/config`

3.  find source's ethernet interface name, it should be of the form `enp*` or
    `eth*`, see `ip address` or `ifconfig` output.

	`source# iface="enp0s29f8u1"` change this

    Fill the target machine's IPv4 address here:

	`source# tgtip="192.168.1.2"` change this


4.  Create netconsole logging target on the source machine:

	`source# modprobe netconsole`

	`source# cd /sys/kernel/config/netconsole`

 	`source# mkdir target1; cd target1`

	`source# srcip=$(ip -4 addr show dev "$iface" | grep -Eo '[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+')`

	`source# echo "$srcip" > local_ip`

	`source# echo "$tgtip" > remote_ip`

	`source# echo "$iface" > dev_name`

	`source# arping -I "$iface" "$tgtip" -f | grep -o '..:..:..:..:..:..' > remote_mac`

	`source# echo 1 > enabled`

5.  Change console loglevel to debugging:

	`source# dmesg -n debug`

6.  Test if the logging works by e.g. inserting or removing an USB
    device on the source. There should be a few lines appearing in the
    terminal, in which you started netcat (nc), on the target host.

7.  Try to reproduce the kernel panic.

Hardware compatibility
----------------------

### What systems are compatible with libreboot?

Any system can easily be added, so *compatibility* merely refers to whatever
boards are integrated in the `lbmk` build system, which libreboot uses.

The [installation page](docs/install/) lists compatible machines.

### Freedom pitfalls with modern Intel hardware {#intel}

Coreboot is nominally Free Software, but requires certain vendor code on some x86
targets that it supports, on both Intel and AMD.

#### Intel Management Engine (ME) {#intelme}

NOTE: The information below is slightly out of date. Nowadays, Intel ME does
not run on an ARC coprocessor, but instead runs on a modified Intel 486 based
architecture, with the ME firmware written for x86 based on the Minix operating
system. However, the overall design philosophy and operation is mostly the
same.

On *most* current Intel platforms that have Intel ME, it is now possible
to disable Intel ME after BringUp. See:

<https://github.com/corna/me_cleaner>\

On all GM45+ICH9M laptops that have an Intel ME in it (additionally, this means
X4X+ICH10 desktops), the ME firmware is not needed in the boot flash. Either a
modified descriptor is used, which disables the ME and removes the region for
it in the boot flash, or a descriptorless setup is used. However, all modern
Intel platforms otherwise require an Intel ME image to be present in the main
boot flash.

Now onto the main topic:

Introduced in June 2006 in Intel's 965 Express Chipset Family of
(Graphics and) Memory Controller Hubs, or (G)MCHs, and the ICH8 I/O
Controller Family, the Intel Management Engine (ME) is a separate
computing environment physically located in the (G)MCH chip. In Q3 2009,
the first generation of Intel Core i3/i5/i7 (Nehalem) CPUs and the 5
Series Chipset family of Platform Controller Hubs, or PCHs, brought a
more tightly integrated ME (now at version 6.0) inside the PCH chip,
which itself replaced the ICH. Thus, the ME is ***present on all Intel
desktop, mobile (laptop), and server systems since mid 2006***.

The ME consists of an ARC processor core (replaced with other processor
cores in later generations of the ME), code and data caches, a timer,
and a secure internal bus to which additional devices are connected,
including a cryptography engine, internal ROM and RAM, memory
controllers, and a ***direct memory access (DMA) engine*** to access the
host operating system's memory as well as to reserve a region of
protected external memory to supplement the ME's limited internal RAM.
The ME also has ***network access*** with its own MAC address through an
Intel Gigabit Ethernet Controller. Its boot program, stored on the
internal ROM, loads a firmware "manifest" from the PC's SPI flash
chip. This manifest is ***signed with a strong cryptographic key***,
which differs between versions of the ME firmware. If the manifest
isn't signed by a specific Intel key, the boot ROM won't load and
execute the firmware and the ME processor core will be halted.

The ME firmware is compressed and consists of modules that are listed in
the manifest along with secure cryptographic hashes of their contents.
One module is the operating system kernel, which is based on a
***proprietary real-time operating system (RTOS) kernel*** called
"ThreadX". The developer, Express Logic, sells licenses and source
code for ThreadX. Customers such as Intel are forbidden from disclosing
or sublicensing the ThreadX source code. Another module is the Dynamic
Application Loader (DAL), which consists of a ***Java virtual machine***
and set of preinstalled Java classes for cryptography, secure storage,
etc. The DAL module can load and execute additional ME modules from the
PC's HDD or SSD. The ME firmware also includes a number of native
application modules within its flash memory space, including Intel
Active Management Technology (AMT), an implementation of a Trusted
Platform Module (TPM), Intel Boot Guard, and audio and video DRM
systems.

The Active Management Technology (AMT) application, part of the Intel
"vPro" brand, is a Web server and application code that enables remote
users to power on, power off, view information about, and otherwise
manage the PC. It can be ***used remotely even while the PC is powered
off*** (via Wake-on-Lan). Traffic is encrypted using SSL/TLS libraries,
but recall that all of the major SSL/TLS implementations have had highly
publicized vulnerabilities. The AMT application itself has ***[known
vulnerabilities](https://en.wikipedia.org/wiki/Intel_Active_Management_Technology#Known_vulnerabilities_and_exploits)***,
which have been exploited to develop rootkits and keyloggers and
covertly gain encrypted access to the management features of a PC.
Remember that the ME has full access to the PC's RAM. This means that
an attacker exploiting any of these vulnerabilities may gain access to
everything on the PC as it runs: all open files, all running
applications, all keys pressed, and more.

[Intel Boot Guard](https://mjg59.dreamwidth.org/33981.md) is an ME
application introduced in Q2 2013 with ME firmware version 9.0 on 4th
Generation Intel Core i3/i5/i7 (Haswell) CPUs. It allows a PC OEM to
generate an asymmetric cryptographic keypair, install the public key in
the CPU, and prevent the CPU from executing boot firmware that isn't
signed with their private key. This means that ***coreboot and libreboot
are impossible to port*** to such PCs, without the OEM's private
signing key. Note that systems assembled from separately purchased
motherboard and CPU parts are unaffected, since the vendor of the
motherboard (on which the boot firmware is stored) can't possibly affect
the public key stored on the CPU.

ME firmware versions 4.0 and later (Intel 4 Series and later chipsets)
include an ME application for ***audio and video
[DRM](https://defectivebydesign.org/what_is_drm_digital_restrictions_management)***
called "Protected Audio Video Path" (PAVP). The ME receives from the
host operating system an encrypted media stream and encrypted key,
decrypts the key, and sends the encrypted media decrypted key to the
GPU, which then decrypts the media. PAVP is also used by another ME
application to draw an authentication PIN pad directly onto the screen.
In this usage, the PAVP application directly controls the graphics that
appear on the PC's screen in a way that the host OS cannot detect. ME
firmware version 7.0 on PCHs with 2nd Generation Intel Core i3/i5/i7
(Sandy Bridge) CPUs replaces PAVP with a similar DRM application called
"Intel Insider". Like the AMT application, these DRM applications,
which in themselves are defective by design, demonstrate the omnipotent
capabilities of the ME: this hardware and its proprietary firmware can
access and control everything that is in RAM and even ***everything that
is shown on the screen***.

The Intel Management Engine with its proprietary firmware has complete
access to and control over the PC: it can power on or shut down the PC,
read all open files, examine all running applications, track all keys
pressed and mouse movements, and even capture or display images on the
screen. And it has a network interface that is demonstrably insecure,
which can allow an attacker on the network to inject rootkits that
completely compromise the PC and can report to the attacker all
activities performed on the PC. It is a threat to freedom, security, and
privacy that can't be ignored.

Before version 6.0 (that is, on systems from 2008/2009 and earlier), the
ME can be disabled by setting a couple of values in the SPI flash
memory. The ME firmware can then be removed entirely from the flash
memory space. The libreboot project does this on
the Intel 4 Series systems that it supports, such as the [ThinkPad
X200](../docs/install/x200.md) and [ThinkPad
T400](../docs/install/t400.md). ME firmware versions 6.0 and
later, which are found on all systems with an Intel Core i3/i5/i7 CPU
and a PCH, include "ME Ignition" firmware that performs some hardware
initialization and power management. If the ME's boot ROM does not find
in the SPI flash memory an ME firmware manifest with a valid Intel
signature, the whole PC will shut down after 30 minutes.

Due to the signature verification, developing free replacement firmware
for the ME is basically impossible. The only entity capable of replacing
the ME firmware is Intel. As previously stated, the ME firmware includes
proprietary code licensed from third parties, so Intel couldn't release
the source code even if they wanted to. And even if they developed
completely new ME firmware without third-party proprietary code and
released its source code, the ME's boot ROM would reject any modified
firmware that isn't signed by Intel. Thus, the ME firmware is both
hopelessly proprietary and "tivoized".

**In summary, the Intel Management Engine and its applications are a
backdoor with total access to and control over the rest of the PC. The
ME is a threat to freedom, security, and privacy, and the libreboot
project strongly recommends avoiding it entirely. Since recent versions
of it can't be removed, this means avoiding all recent generations of
Intel hardware.**

The *above* paragraph is only talking about setups where the *full* Intel ME
firmware is used, containing networking code and especially *Active Management
Technology* (AMT).

Use of the `me_cleaner` utility is believed to minimize any security risk when
using these Intel platforms, and coreboot *does* contain fully free code for
sandybridge/ivybridge platforms.

More information about the Management Engine can be found on various Web
sites, including [me.bios.io](http://me.bios.io/Main_Page),
[unhuffme](http://io.netgarage.org/me/), [coreboot
wiki](http://www.coreboot.org/Intel_Management_Engine), and
[Wikipedia](https://en.wikipedia.org/wiki/Intel_Active_Management_Technology).
The book ***[Platform Embedded Security Technology
Revealed](https://www.apress.com/9781430265719)*** describes in great
detail the ME's hardware architecture and firmware application modules.

If you're stuck with the ME (non-libreboot system), you might find this
interesting:
<https://hardenedlinux.github.io/firmware/2016/11/17/neutralize_ME_firmware_on_sandybridge_and_ivybridge.html>

#### Firmware Support Package (FSP) {#fsp}

On all recent Intel systems, coreboot support has revolved around
integrating a vendor file (for each system) called the *FSP* (firmware support
package), which handles all of the hardware initialization, including
memory and CPU initialization. Reverse engineering and replacing this
file is almost impossible, due to how complex it is. Even for the most
skilled developer, it would take years to replace. Intel distributes
this file to firmware developers, for free redistribution.

Since the FSP is responsible for the early hardware initialization, that
means it also handles SMM (System Management Mode). This is a special
mode that operates below the operating system level.

#### CPU microcode updates {#microcode}

The microcode configures logic gates in your CPU, to implement an instruction
set architecture. Your CPU will already contain them, but it also supplies a
way to update the microcode at boot time, fixing bugs and greatly enhancing
the general reliability of your system.

Microcode is already discussed in great detail, on the [binary blobs
policy](news/policy.md).

This interesting video talks about how a group of people reverse engineered
the microcode on AMD processors:

<https://yewtu.be/watch?v=W3FbTMqYi4U>

Here is another video:

<https://yewtu.be/watch?v=I6dQfnb3y0I>

The git repository for that project is here:

<https://github.com/RUB-SysSec/Microcode>

Both the video and the repository give some further insight about CPU
microcode. The way it works on AMD will be very similar to Intel.

### Freedom pitfalls to consider on AMD hardware {#amd}

NOTE: Nowadays there's openSIL <https://github.com/openSIL/openSIL> - it's
AMD's attempt to provide some source code again, that projects like coreboot
can use, but AMD is still problematic; the PSP for example (see below) cannot
be "neutered" (nothing like `me_cleaner`, or *psp\_cleaner*) exists yet.

AMD has more or less the same problem as Intel, when it comes to software
freedom.

#### AMD Platform Security Processor (PSP) 

This is basically AMD's own version of the [Intel Management
Engine](#intelme). It has all of the same basic security and freedom
issues, although the implementation is wildly different.

The Platform Security Processor (PSP) is built in on the AMD CPUs whose
[architecture](https://en.wikipedia.org/wiki/List_of_AMD_CPU_microarchitectures) is Late Family 16h (Puma), Zen 17h or later (and also on
the AMD GPUs which are GCN 5th gen (Vega) or later). On the CPUs, a PSP
controls the main x86 core startup. PSP firmware is cryptographically
signed with a strong key similar to the Intel ME. If the PSP firmware
is not present, or if the AMD signing key is not present, the x86 cores
will not be released from reset, rendering the system inoperable.

The PSP is an ARM core with TrustZone technology, built onto the main
CPU die. As such, it has the ability to hide its own program code,
scratch RAM, and any data it may have taken and stored from the
lesser-privileged x86 system RAM (kernel encryption keys, login data,
browsing history, keystrokes, who knows!). To make matters worse, the
PSP theoretically has access to the entire system memory space (AMD
either will not or cannot deny this, and it would seem to be required to
allow the DRM "features" to work as intended), which means that it has
at minimum MMIO-based access to the network controllers and any other
PCI/PCIe peripherals installed on the system.

In theory any malicious entity with access to the AMD signing key would
be able to install persistent malware that could not be eradicated
without an external flasher and a known good PSP image. Furthermore,
multiple security vulnerabilities have been demonstrated in AMD firmware
in the past, and there is every reason to assume one or more zero day
vulnerabilities are lurking in the PSP firmware. Given the extreme
privilege level (ring -2 or ring -3) of the PSP, said vulnerabilities
would have the ability to remotely monitor and control any PSP enabled
machine completely outside of the user's knowledge.

Much like with the Intel Boot Guard (an application of the Intel
Management Engine), AMD's PSP can also act as a tyrant by checking
signatures on any boot firmware that you flash, making replacement boot
firmware (e.g. libreboot, coreboot) impossible on some boards. Early
anecdotal reports indicate that AMD's boot guard counterpart will be
used on most OEM hardware, disabled only on so-called "enthusiast"
CPUs.

#### AMD IMC firmware 

Read <https://www.coreboot.org/AMD_IMC>.

NOTE: This section is oudated, and it is in need of cleanup.

#### AMD SMU firmware 

NOTE: This section may be outdated, and it is in need of cleanup.

Handles some power management for PCIe devices (without this, your
laptop will not work properly) and several other power management
related features.

The firmware is signed, although on older AMD hardware it is a symmetric
key, which means that with access to the key (if leaked) you could sign
your own modified version and run it. Rudolf Marek (coreboot hacker)
found out how to extract this key [in this video
demonstration](https://media.ccc.de/v/31c3_-_6103_-_en_-_saal_2_-_201412272145_-_amd_x86_smu_firmware_analysis_-_rudolf_marek),
and based on this work, Damien Zammit (another coreboot hacker)
[partially replaced it](https://github.com/zamaudio/smutool/) with free
firmware, but on the relevant system (ASUS F2A85-M) there were still
other such files present (Video BIOS, and others).

#### AMD AGESA firmware 

NOTE: More needs to be written about this, to reflect the current reality.
The situation with AMD has evolved in recent years. The information on this FAQ
page is a few years out of date.

This is responsible for virtually all core hardware initialization on
modern AMD systems. In 2011, AMD started cooperating with the coreboot
project, releasing this as source code under a free license. In 2014,
they stopped releasing source code and started releasing AGESA as vendor
blobs instead. This makes AGESA now equivalent to [Intel FSP](#fsp).

#### AMD CPU microcode updates 

Read the Intel section 
practically the same, though it was found with much later hardware in
AMD that you could run without microcode updates. It's unknown whether
the updates are needed on all AMD boards (depends on CPU).

The libreboot project does not consider microcode updates a problem, and it
enables them by default on all supported hardware.

### Hi, I have &lt;insert random system here&gt;, is it supported?

If it's supported by coreboot, you can add it immediately.
Read the [porting guide](/docs/maintain/porting.html) for how to port for a new board.
If you are able to generate a working rom for your system, please read
[lbmk maintenance manual](docs/maintain/) for how to add it to libreboot.

If coreboot lacks support for your hardware, you must add support for it.
Please consult the coreboot project for guidance.

General questions
-----------------

### How do I install libreboot?

See [installation guide](docs/install/)

### How do I program an SPI flash chip?

Refer to:\
[Externally rewrite 25xx NOR flash via SPI protocol](docs/install/spi.md)

It's possible to use a 16-pin SOIC test clip on an 8-pin SOIC chip, if you
align the pins properly. The connection is generally more sturdy.

### How do I write-protect the flash chip?

By default, there is no write-protection on a libreboot system. This is
for usability reasons, because most people do not have easy access to an
external programmer for re-flashing their firmware, or they find it
inconvenient to use an external programmer.

On some systems, it is possible to write-protect the firmware, such that
it is rendered read-only at the OS level (external flashing is still
possible, using dedicated hardware). [See instructions
here.](docs/linux/grub_hardening.md#flash-write-protection)

TODO: Document PRx based flash protection on Intel platforms, and investigate
other methods on AMD systems.

### How do I change the BIOS settings?

Most libreboot setups actually use the [GRUB
payload](http://www.coreboot.org/GRUB2). More information about payloads
can be found at
[coreboot.org/Payloads](http://www.coreboot.org/Payloads). SeaBIOS is also
available. The *CMOS* config is hardcoded in libreboot.

The libreboot project inherits the modular payload concept from coreboot, which
means that pre-OS bare-metal *BIOS setup* programs are not very
practical. Coreboot (and libreboot) does include a utility called
*nvramtool*, which can be used to change some settings. You can find
nvramtool under *coreboot/util/nvramtool/*, in the libreboot source
archives.

The *-a* option in nvramtool will list the available options, and *-w*
can be used to change them. Consult the nvramtool documentation on the
coreboot wiki for more information.

In practise, you don't need to change any of those settings, in most
cases.

Default libreboot setups lock the CMOS table, to ensure consistent functionality
for all users. You can use:

	nvramtool -C yourrom.rom -w somesetting=somevalue

To get a full list of available options, do this:

	nvramtool -C yourrom.rom -a

This will change the default inside that ROM image, and then you can
re-flash it.

### How do I pad a ROM before flashing?

It is advisable to simply use a larger ROM image. This section was written
mostly for ASUS KCMA-D8 and KGPE-D16 motherboards, where previously we only
provided 2MiB ROM images in libreboot, but we now provide 16MiB ROM images.
Other sizes are not provided because in practise, someone upgrading one of
these chips will just use a 16MiB one. Larger sizes are available, but 16MiB
is the maximum that you can use on all currently supported libreboot systems
that use SPI flash.

Required for ROMs where the ROM image is smaller than the flash chip
(e.g. writing a 2MiB ROM to a 16MiB flash chip).

Create an empty (00 bytes) file with a size the difference between
the ROM and flash chip. The case above, for example:

	truncate -s +14MiB pad.bin

For x86 descriptorless images you need to pad from the *beginning* of the ROM:

	cat pad.bin yourrom.rom > yourrom.rom.new

For ARM and x86 with intel flash descriptor, you need to pad after the image:

	cat yourrom.rom pad.bin > yourrom.rom.new

Flash the resulting file. Note that cbfstool will not be able to
operate on images padded this way so make sure to make all changes to
the image, including runtime config, before padding.

To remove padding, for example after reading it off the flash chip,
simply use dd(1) to extract only the non-padded portion. Continuing with the
examples above, in order to extract a 2MiB x86 descriptorless ROM from a
padded 16MiB image do the following:

	dd if=flashprogread.rom of=yourrom.rom ibs=14MiB skip=1

With padding removed cbfstool will be able to operate on the image as usual.

### Do I need to install a bootloader when installing a distribution?

Most libreboot setups integrate the GRUB bootloader already, as a
*[payload](http://www.coreboot.org/Payloads)*. This means that the GRUB
bootloader is actually *flashed*, as part of the boot firmware
(libreboot). This means that you do not have to install a boot loader on
the HDD or SSD, when installing a new distribution. You'll be able to
boot just fine, using the bootloader (GRUB) that is in the flash chip.

This also means that even if you remove the HDD or SSD, you'll still
have a functioning bootloader installed which could be used to boot a
live distribution installer from a USB flash drive. See
[How to install Linux on a libreboot system](../docs/linux/grub_boot_installer.md)

Nowadays, other payloads are also provided. If you're using the SeaBIOS payload,
then the normal MBR bootsector is used on your HDD or SSD, like you would
expect. So the above paragraphs only apply to the GRUB payload.

### Do I need to re-flash when I re-install a distribution?

Not anymore. Recent versions of libreboot (using the GRUB payload) will
automatically switch to a GRUB configuration on the HDD or SSD, if it
exists. You can also load a different GRUB configuration, from any kind
of device that is supported in GRUB (such as a USB flash drive). For
more information, see
[Modifying the GRUB Configuration in libreboot Systems](../docs/linux/grub_cbfs.md)

If you're using the SeaBIOS payload, it's even easier. It works just like you
would expect. SeaBIOS implements a normal x86 BIOS interface.

### What does a flash chip look like?

You can find photos of various chip types on the following page:\
[External 25xx NOR flashing guide](docs/install/spi.md)

Inability to load `thinkpad_acpi` on post-haswell systems {#thinkpad-acpi}
--------------------------------------------------------------------------

Reported by a user on Debian 11 (on a ThinkPad T440p) and a user
on Void Linux (ThinkPad T480), Linux (or modprobe) may fail to load
`thinkpad_acpi`.

```
modprobe: ERROR: could not insert 'thinkpad_acpi': "No such device"
```

It is suspected that at least these motherboards are affected:

 * [ThinkPad W541](docs/install/w541_external.md)
 * [ThinkPad T440p](docs/install/t440p_external.md)
 * [ThinkPad T480/T480s](docs/install/t480.md)

This may result the following effects, including or not limited to:

 * Temperature reporting not working
 * Battery info not working.
 * Fan speed reporting not working
 * Fan control not working

**However, enabling it on the ThinkPad T480 may cause the following issues,
as of Libreboot 20241206rev8: it might turn off rfkill making you have to
manually unblock wlan, and the Fn keys through F9 to F12 may stop working.**

For these systems, add the line

```
options thinkpad_acpi force_load=1
```

to any file in `/etc/modprobe.d`. You can also add

```
thinkpad_acpi.force_load=1
```

to your kernel parameters (in GRUB, or your preferred linux-capable bootloader).

### tlp

You can install the `tlp` package and start that service. For example, on
Debian:

```
apt-get install tlp tlp-rdw
systemctl enable tlp
systemctl start tlp
```

Now read the manual:

```
man tlp-stat
```

As root, you can do:

```
tlp-stat -b
```

This will provide information about the battery.

What other firmware exists outside of libreboot?
------------------------------------------------

You can also read information about these in the [libreboot binary blob
reduction policy](news/policy.md), where it goes into more detail about some
of them.

### External GPUs

The Video BIOS is present on most video cards. For integrated graphics,
the VBIOS (special kind of OptionROM) is usually embedded
in the main boot firmware. For external graphics, the VBIOS is
usually on the graphics card itself. This is usually proprietary; the
only difference is that SeaBIOS can execute it (alternatively, you embed it
in a coreboot ROM image and have coreboot executes it, if you use a
different payload, such as GRUB).

The *coreboot project* provides free initialization code, on many boards, and
libreboot will use this code when it is available, depending on the configuration.

In configurations where SeaBIOS and native GPU init are used together,
a special shim VBIOS is added that uses coreboot linear framebuffer.

### EC (embedded controller) firmware 

Most (all?) laptops have this. The EC (embedded controller) is a small,
separate processor that basically processes inputs/outputs that are
specific to laptops. For example:

-   When you flick the radio on/off switch, the EC will enable/disable
    the wireless devices (wifi, bluetooth, etc) and enable/disable an
    LED that indicates whether it's turned on or not
-   Listen to another chip that produces temperature readings, adjusting
    fan speeds accordingly (or turning the fan(s) on/off).
-   Takes certain inputs from the keyboard, e.g. brightness up/down,
    volume up/down.
-   Detect when the lid is closed or opened, and send a signal
    indicating this.
-   Etc.

EC is present on nearly all laptops. Other devices use, depending on complexity,
either EC or variant with firmware in Mask ROM - SuperIO.

### HDD/SSD firmware 

HDDs and SSDs have firmware in them, intended to handle the internal
workings of the device while exposing a simple, standard interface (such
as AHCI/SATA) that the OS software can use, generically. This firmware
is transparent to the user of the drive.

HDDs and SSDs are quite complex, and these days contain quite complex
hardware which is even capable of running an entire operating system (by
this, we mean that the drive itself is capable of running its own
embedded OS), even Linux.

SSDs and HDDs are a special case, since they are persistent storage
devices as well as computers.

Example attack that malicious firmware could do: substitute your SSH
keys, allowing unauthorized remote access by an unknown adversary. Or
maybe substitute your GPG keys. SATA drives can also have DMA (through
the controller), which means that they could read from system memory;
the drive can have its own hidden storage, theoretically, where it could
read your LUKS keys and store them unencrypted for future retrieval by
an adversary.

With proper IOMMU and use of USB instead of SATA, it might be possible
to mitigate any DMA-related issues that could arise.

Some proof of concepts have been demonstrated. For HDDs:
<https://spritesmods.com/?art=hddhack&page=1> For SSDs:
<http://www.bunniestudios.com/blog/?p=3554>

Viable free replacement firmware is currently unknown to exist. For
SSDs, the
[OpenSSD](https://web.archive.org/web/20220425071606/http://www.openssd-project.org/wiki/The_OpenSSD_Project)
project may be interesting.

Apparently, SATA drives themselves don't have DMA but can make use of
it through the controller. This
<http://web.archive.org/web/20170319043915/http://www.lttconn.com/res/lttconn/pdres/201005/20100521170123066.pdf>
(pages 388-414, 420-421, 427, 446-465, 492-522, 631-638) and this
<http://www.intel.co.uk/content/dam/www/public/us/en/documents/technical-specifications/serial-ata-ahci-spec-rev1_3.pdf>
(pages 59, 67, 94, 99).

The following is based on discussion with Peter Stuge (CareBear\\) in
the coreboot IRC channel on Friday, 18 September 2015, when
investigating whether the SATA drive itself can make use of DMA. The
following is based on the datasheets linked above:

According to those linked documents, FIS type 39h is *"DMA Activate FIS
- Device to Host"*. It mentions *"transfer of data from the host to
the device, and goes on to say: Upon receiving a DMA Activate, if the
host adapter's DMA controller has been programmed and armed, the host
adapter shall initiate the transmission of a Data FIS and shall transmit
in this FIS the data corresponding to the host memory regions indicated
by the DMA controller's context."* FIS is a protocol unit (Frame
Information Structure). Based on this, it seems that a drive can tell
the host controller that it would like for DMA to happen, but unless the
host software has already or will in the future set up this DMA transfer
then nothing happens. **A drive can also send DMA Setup**. If a DMA
Setup FIS is sent first, with the Auto-Activate bit set, then it is
already set up, and the drive can initiate DMA. The document goes on to
say *"Upon receiving a DMA Setup, the receiver of the FIS shall
validate the received DMA Setup request."* - in other words, the host
is supposed to validate; but maybe there's a bug there. The document
goes on to say *"The specific implementation of the buffer identifier
and buffer/address validation is not specified"* - so noone will
actually bother. *"the receiver of the FIS"* - in the case we're
considering, that's the host controller hardware in the chipset and/or
the kernel driver (most likely the kernel driver). All SATA devices have
flash-upgradeable firmware, which can usually be updated by running
software in your operating system; **malicious software running as root
could update this firmware, or the firmware could already be
malicious**. Your HDD or SSD is the perfect place for a malicious
adversary to install malware, because it's a persistent storage device
as well as a computer.

Based on this, it's safe to say that use of USB instead of SATA is
advisable if security is a concern. USB 2.0 has plenty of bandwidth for
many HDDs (a few high-end ones can use more bandwidth than USB 2.0 is
capable of), but for SSDs it might be problematic. USB 3.0 will provide more
reasonable performance, though note that depending on the system, you may have
to deal with binary vendor XHCI firmware in your kernel (if that bothers you).

Use of USB is also not an absolute guarantee of safety, so do beware.
The attack surface becomes much smaller, but a malicious drive could
still attempt a "fuzzing" attack (e.g. sending malformed USB
descriptors, which is how the tyrant DRM on the Playstation 3 was
broken, so that users could run their own operating system and run
unsigned code). (you're probably safe, unless there's a security flaw
in the USB library/driver that your OS uses. USB is generally considered
one of the safest protocols, precisely because USB devices have no DMA)

Other links:

-   <https://www.vice.com/en_us/article/ypwkwk/the-nsas-undetectable-hard-drive-hack-was-first-demonstrated-a-year-ago>

It is recommended that you use full disk encryption, on HDDs connected
via USB. There are several adapters available online, that allow you to
connect SATA HDDs via USB, and Libreboot is capable of booting from them the
normal way. Consult the documentation for your Linux/BSD operating system, so
that you can know how to install it with *full disk encryption*.

The current theory (unproven) is that this will at least prevent
malicious drives from wrongly manipulating data being read from or
written to the drive, since it can't access your LUKS key if it's only
ever in RAM, provided that the HDD doesn't have DMA (USB devices don't
have DMA). The worst that it could do in this case is destroy your data.
Of course, you should make sure never to put any keyfiles in the LUKS
header. **Take what this paragraph says with a pinch of salt. This is
still under discussion, and none of this is proven.**

### NIC (ethernet controller) 

Ethernet NICs will typically run firmware inside, which is responsible
for initializing the device internally. Theoretically, it could be
configured to drop packets, or even modify them.

With proper IOMMU, it might be possible to mitigate the DMA-related
issues. A USB NIC can also be used, which does not have DMA.

### CPU microcode 

Microcode configures logic gate arrays in a microprocessor, to implement the
instruction set architecture. Special *decoders* in the microprocessor will
configure the circuitry, based on that microcode.

The [libreboot blob reduction policy](news/policy.md) goes into great detail
about microcode.

### Sound card 

Sound hardware (integrated or discrete) typically has firmware on it
(DSP) for processing input/output. Again, a USB DAC is a good
workaround.

### Webcam 

Webcams have firmware integrated into them that process the image input
into the camera; adjusting focus, white balancing and so on. Can use USB
webcam hardware, to work around potential DMA issues; integrated webcams
(on laptops, for instance) are discouraged by the libreboot project, for
security reasons.

### USB host controller 

USB host controllers require firmware. Sometimes, this has to be supplied
by coreboot itself.

### WWAN firmware 

Some laptops might have a simcard reader in them, with a card for
handling WWAN, connecting to a 3g/4g (e.g. GSM) network. This is the
same technology used in mobile phones, for remote network access (e.g.
internet).

NOTE: not to be confused with wifi. Wifi is a different technology, and
entirely unrelated.

The baseband processor inside the WWAN chip will have its own embedded
operating system, most likely proprietary. Use of this technology also
implies the same privacy issues as with mobile phones (remote tracking
by the GSM network, by triangulating the signal).

On some laptops, these cards use USB (internally), so won't have DMA,
but it's still a massive freedom and privacy issue. If you have an
internal WWAN chip/card, the libreboot project recommends that you
disable and (ideally, if possible) physically remove the hardware. If
you absolutely must use this technology, an external USB dongle is much
better because it can be easily removed when you don't need it, thereby
disabling any external entities from tracking your location.

Use of ethernet or wifi is recommended, as opposed to mobile networks,
as these are generally much safer.

Operating Systems
-----------------

### Can I use Linux?

Absolutely! It is well-tested in libreboot, and highly recommended. See
[installing Linux](../docs/linux/grub_boot_installer.md) and
[booting Linux](../docs/linux/grub_cbfs.md).

Any recent distribution should work, as long as it uses KMS (kernel mode
setting) for the graphics.

### Fedora won't boot? (may also be applicable to Redhat/CentOS)

On Fedora, by default the grub.cfg tries to boot linux in 16-bit mode. You
just have to modify Fedora's GRUB configuration.
Refer to [the Linux page](docs/linux/).

### Can I use BSD?

Absolutely! The libreboot firmware has good support for FreeBSD, NetBSD and
OpenBSD. Other systems are untested, but should work just fine.

See:
[docs/bsd/](docs/bsd/)

### Windows??

Yes, you can use Windows 10 and 11. They are not officially supported and the
Libreboot project recommends that you *avoid* this choice, because Windows is
proprietary software. What this means is that you do *not* have the freedom
to use, study, adapt and share the software in any reasonable way. You are
entirely at the mercy of Microsoft, whose motive is profit, as opposed to the
general advancement of computer science and the welfare of everyone.

See: [What is Free Software?](https://writefreesoftware.org/learn)

In addition to being proprietary software, Windows is known to be full of bugs,
including *backdoors*. When you *use* Windows, it will send information about
you to third parties, used for a variety of purposes such as advertising, but
it's quite possible that three-letter agencies may also receive your data if
you use Windows.

For the sake of your freedom, you should never, ever use Windows. Use Linux or
BSD systems, which are well-supported. Anyway:

[Video of Windows 10 booting up](https://yewtu.be/watch?v=BWq6XnWKQnM)

[Video of Windows 11 booting up](https://yewtu.be/watch?v=OFHiMfVNNeA)

Of note: Windows 11 officially requires at least TPM 1.2 to be supported, and
it requires use of UEFI SecureBoot. To bypass this requirement, MajorGeeks has
a guide, see: <https://www.majorgeeks.com/content/page/bypass_tpm.html>

The person who tested this also stated that they were unable to perform an
in-place upgrade from 10 to 11, so they had to wipe the drive and perform a
clean (note: not clean, because Windows is full of NSA spyware) installation.

In both cases, as shown above, the Windows operating system was booting from
SeaBIOS, with the coreboot framebuffer initialised at startup, on an Intel GPU
initialised via coreboot's *libgfxinit*, on November 2023 versions of Libreboot.
We do not yet support booting with UEFI on x86 machines.

### Are other operating systems compatible?

Unknown. Perhaps so, but it's impossible to say without further testing.

What level of software freedom does libreboot give me?
--------------------------------------------------

Please read the [libreboot binary blob minimalisation policy](news/policy.md).

Please also read:
[Software and hardware freedom status for each motherboard supported by
Libreboot](software-freedom.md)

The libreboot firmware provides host hardware initialisation inside ROM files,
that can be written to NOR flash, but on many systems there exist
a lot more small computers on the motherboard running blob firmware.
Some of them are not practicable to replace due to being located on Mask ROM.
Most laptops have EC (Embedded Controller) firmware, for example.

Besides software itself (embedded in ROM or not), most hardware
(from ICs to circuit boards) are not released under open source licenses.
We do not have a single device that can be considered be "100% free",
and such absolutes are nearly impossible to reach.

Notable vendor code present (example) (not a complete list):

* All devices
	* SATA/PATA Hard Drive/Optical Disc Drive Firmware
  ([often contain powerful ARM based computer](
  http://spritesmods.com/?art=hddhack&page=1))
	* Pendrives and any USB peripherals - they contain a computer
  with code running to at the very least handle the USB protocol
* ThinkPads:
	* EC Firmware (H8S until including Sandy Bridge, later ARC based MEC16xx)
	* TrackPoint Firmware (8051)
	* Penabled devices contain µPD78F0514 MCU on wacom subboard,
  and Atmega (AVR) on led indicator/button board
	* Battery BMS, bq8030 (CoolRISC C816)
* Chomebooks C201PA/C100PA:
	* Battery BMS, bq30z55
	* Elan Touchpad
	* eMMC [flash memory controller](https://en.wikipedia.org/wiki/Flash_memory_controller) firmware

One day, we will live in a world where anyone can get their own chips made,
including CPUs but also every other type of IC. Efforts to make homemade
chip fabrication a reality are now in their infancy, but such efforts do
exist, for example, the work done by Sam Zeloof and the Libre Silicon project:

* <https://www.youtube.com/channel/UC7E8-0Ou69hwScPW1_fQApA>
* <http://sam.zeloof.xyz/>
* <https://libresilicon.com/>

(Sam literally makes CPUs in his garage)

Where can I learn more about electronics
----------------------------------------

* Basics of soldering and rework by PACE  
    Both series of videos are mandatory regardless of your soldering skill.
    * [Basic Soldering](https://yewtu.be/playlist?list=PL926EC0F1F93C1837)
    * [Rework and Repair](https://yewtu.be/playlist?list=PL958FF32927823D12)
    The PACE series above covers classic techniques, but does not cover much
    about *modern* electronics. For that, see:
    * [iFixit microsoldering lessons, featuring Jessa
	Jones](https://yewtu.be/playlist?list=PL4INaL5vWobD_CltiZXr7K46oJ33KvwBt)
    * Also see youtube links below, especially Louis Rossman videos, to learn
      a (lot) more.
* [edX course on basics of electronics](https://www.edx.org/course/circuits-and-electronics-1-basic-circuit-analysi-2)  
    In most countries contents of this course is covered during
    middle and high school. It will also serve well to refresh your memory
    if you haven't used that knowledge ever since.
* Impedance intro
    * [Similiarities of Wave Behavior](https://yewtu.be/watch?v=DovunOxlY1k)
    * [Reflections in tranmission line](https://yewtu.be/watch?v=y8GMH7vMAsQ)
    * Stubs:
        * [Wikipedia article on stubs](https://en.wikipedia.org/wiki/Stub_(electronics))
        * [Polar Instruments article on stubs](http://www.polarinstruments.com/support/si/AP8166.html)  
        With external SPI flashing we only care about unintended PCB stubs
* [How to accurately measure header/connector pitch](https://www.microcontrollertips.com/accurately-measure-headerconnector-pitch/)
* Other YouTube channels with useful content about electronics
    * [EEVblog](https://yewtu.be/channel/UC2DjFE7Xf11URZqWBigcVOQ)
	(generally about electronics, reviews about equipment, etc, some
	repair videos)
    * [Louis Rossmann](https://yewtu.be/channel/UCl2mFZoRqjw_ELax4Yisf6w)
	(right to repair advocacy, lots of macbook repair videos)
    * [mikeselectricstuff](https://yewtu.be/channel/UCcs0ZkP_as4PpHDhFcmCHyA)
    * [bigclive](https://yewtu.be/channel/UCtM5z2gkrGRuWd0JQMx76qA)
    * [ElectroBOOM](https://yewtu.be/channel/UCJ0-OtVpF0wOKEqT2Z1HEtA)
	(he blows stuff up, and shows you how not to do that)
    * [Jeri Ellsworth](https://yewtu.be/user/jeriellsworth/playlists)
	(has a video showing how to make a *transistor* yourself)
    * [Sam Zeloof](https://yewtu.be/channel/UC7E8-0Ou69hwScPW1_fQApA)
	(Sam literally makes CPUs in his garage, inspired by Jeri Ellsworth's
	work with transistors)
    * [Ben Eater](https://eater.net/) (shows how to build an 8-bit CPU from scratch,
	also does things with MOS 6502)
	(also shows how to make other things like graphics chips, teaches networking
	concepts) - check out Ben's videos! <https://redirect.invidious.io/beneater>
    * [iPad Rehab with Jessa Jones](https://yewtu.be/channel/UCPjp41qeXe1o_lp1US9TpWA)
	(very precise soldering. she does repairs on mobile phones and such, also
	featured in iFixit's series about getting into component repairs)
* Boardview files can be open with [OpenBoardview](https://github.com/OpenBoardView/OpenBoardView),
which is libre software under MIT license.

Use of `yt-dlp` (an enhanced fork of `youtube-dl`) is recommended for links
to `youtube.com`. See: <https://github.com/yt-dlp/yt-dlp>

Lastly the most important message to everybody gaining this wonderful new hobby - [Secret to Learning Electronics](https://yewtu.be/watch?v=xhQ7d3BK3KQ)
