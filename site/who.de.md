---
title: Who develops libreboot?
x-toc-enable: true
...

Der Zweck dieser Seite ist klar zu definieren wer am Libreboot Projekt
arbeitet, wer das Projekt betreibt, wie Entscheidungen getroffen werden, 
und wie das Projekt grundsätzlich funktioniert.

Informationen über die wichtigsten Beteiligten an Libreboot sind auf
dieser Seite zu finden, 
dort sind diese Leute aufgeführt: [Liste der Beteiligten](contrib.md)

Leah Rowe (Gründerin, Chef Entwicklerin)
===================================

Leah Rowe ist die Gründerin des Libreboot Projekts. Leah beaufsichtigt
die gesamte Libreboot Entwicklung, überprüft 
externe Beiträge, and hat bei Entscheidungen das letzte Wort.
Leah ist Eigentümerin der libreboot.org Server, und betreibt diese von
ihrem Labor in Großbrittanien.

Wenn Du mehr über Leah's Mitwirken an Libreboot erfahren möchtest, dann
kannst Du ihren Eintrag unter der 
[Seite mit allen Mitwirkenden, Vergangenheit und Gegenwart](contrib.md)
lesen.

Caleb La Grange
===============

Caleb ist online auch bekannt unter `shmalebx9`.
Caleb kümmert sich hauptsächlich um Verbesserungen am lbmk Build System,
das Hinzufügen neuer Boards sowie um die Dokumentation.

Wenn Du mehr über Caleb's Mitwirken an Libreboot erfahren möchtest, dann
kannst Du seinen Eintrag unter der 
[Seite mit allen Mitwirkenden, Vergangenheit und Gegenwart](contrib.md)
lesen.

Alper Nebi Yasak
================

Alper kümmert sich um die Entwicklung der U-Boot Payload Unterstützung 
in Libreboot, koordiniert Tests auf Chromebooks die dies nutzen, und
erledigt jegliche notwendige Upstream Arbeit an U-Boot selbst.
`alpernebbi` bei Libera IRC.

Wenn Du mehr über Alper's Mitwirken an Libreboot erfahren möchtest, dann
kannst Du seinen Eintrag unter der 
[Seite mit allen Mitwirkenden, Vergangenheit und Gegenwart](contrib.md)
lesen.

Entwickler gesucht!
==================

**Lerne wie Du Patches beisteuern kannst unter der [git Seite](git.de.md)**

Jeder ist willkommen an der Entwicklung teilzunehmen.
