---
title: свободную, с открытым исходным кодом BIOS/UEFI прошивка
x-toc-enable: true
...

Проект *Libreboot* предоставляет [свободную, с открытым исходным кодом](https://writefreesoftware.org/) загрузочную прошивку, основанную на coreboot, заменяющую проприетарные BIOS/UEFI на [некоторых Intel/AMD x86 и ARM материнских платах](docs/install/#which-systems-are-supported-by-libreboot), включая ноутбуки и десктопы. Она инициализирует аппаратное обеспечение компьютера (например, контроллер памяти, процессор, переферию) и запускает загрузчик для вашей операционный системы. [Linux](docs/linux/) и [BSD](docs/bsd/) хорошо поддерживаются. Можно попросить помощь через [\#libreboot](https://web.libera.chat/#libreboot) на [Libera](https://libera.chat/) IRC.

<img tabindex=1 class="r" src="https://av.libreboot.org/t480/t480.jpg" /><span class="f"><img src="https://av.libreboot.org/t480/t480.jpg" /></span>

Libreboot provides GNU boot loader "[GRUB](docs/linux/)" and SeaBIOS payloads
on x86/x86\_64
Intel/AMD motherboards, and a [U-Boot UEFI payload](docs/uboot/) *for coreboot*
on ARM64(Aarch64) motherboards.
An [x86/x86\_64 U-Boot UEFI payload](docs/uboot/uboot-x86.md) is also available
on some boards. The x86, x86\_64 and arm64 U-Boot payloads provide a lightweight
UEFI boot implementation, which can boot many Linux distros and BSD systems.
The SeaBIOS and GRUB payloads also boot Linux/BSD systems. Which one you use
depends on your preferences. Libreboot's design incorporates all of these boot
methods in a single image, so you can choose which one you use at boot time,
and more payloads (e.g. Linux kexec payload) are planned for future releases.

**НОВЫЙ РЕЛИЗ: Последний релиз Libreboot - 20241206, опубликован 6 December 2024 года. Смотрите: [Libreboot 20241206 release announcment](news/libreboot20241206.md).**

Вы также можете купить [Libreboot предустановленным](https://minifree.org) от Minifree Ltd, а также присылать нам свои совместимые устройства для [установки Libreboot](https://minifree.org/product/installation-service/). Основатель и ведущий разработчик Libreboot, Лия Роу, также владеет и управляет Minifree; продажи обеспечивают финансирование для Libreboot.

*Мы* верим, что свобода [изучать, делиться, модифицировать и использовать программное обеспечение](https://writefreesoftware.org/) без каких-либо ограничений, является одним из основных человеческих прав, который должен иметь каждый. В этом контексте, *свобода программного обеспечения* важна. Ваша свобода важна. Образование важно. [Право на ремонт](https://en.wikipedia.org/wiki/Right_to_repair) важно; Libreboot позволяет вам продолжить использовать ваше оборудование. Вот *почему* Libreboot существует.

Обзор устройства Libreboot
--------------------------

<img tabindex=1 class="l" src="https://av.libreboot.org/hp9470m/9470m+2560p.jpg" /><span class="f"><img src="https://av.libreboot.org/hp9470m/9470m+2560p.jpg" /></span>

Libreboot предоставляет [coreboot](https://coreboot.org/) для [инициализации машины](https://doc.coreboot.org/getting_started/architecture.html), которая затем переходит к [полезной нагрузке](https://doc.coreboot.org/payloads.html) в загрузочной флэш-памяти; coreboot работает со многими программами, но Libreboot предоставляет только SeaBIOS, GRUB и U-Boot. Memtest86+ также предоставляется, но только на некоторых материнских платах. Полезная нагрузка - программа во флэш-памяти, которая предоставляет "ранний" пользовательский интерфейс для загрузки операционной системы. Это значит, что вы можете запустить все что угодно из загрузочной флэш-памяти (даже Linux!).

Libreboot - это *дистрибутив coreboot* также, как и Debian - *диструбутив Linux*. Libreboot делает coreboot простым к использованию для обычных пользователей, предоставляя [полностью автоматизированную систему сборки](docs/build/) и [дружелюбные к пользователю инструкции к установке](docs/install/), в дополении к регулярному бинарному релизу, дающему скомпилированные ROM образы для установки на поддерживаемом оборудовании. Без автоматизации, предоставляемой Libreboot, coreboot был бы недоступен для большинства пользователей; однако вы также можете [сконфигурировать](docs/mantain) Libreboot как вы желаете.

Почему стоит использовать Libreboot?
------------------------------------

<img tabindex=1 class="r" style="max-width:25%;" src="https://av.libreboot.org/thinkpadcollection/thinkpadcollection1-min.jpg" /><span class="f"><img src="https://av.libreboot.org/thinkpadcollection/thinkpadcollection1-min.jpg" /></span>

Если вы уже склоняетесь к свободному программному обеспечению, возможно, вы уже пользователь coreboot, Libreboot помогает проще начать использовать или поддерживать coreboot на вашей машине, с помощью автоматизированной сборки. Мы предоставляем регулярные протестированные релизы, предварительно собранные, часто с некоторыми патчами coreboot (и другим кодом) для того, чтобы гарантировать стабильность. Для сравнения, coreboot использует модель плавающих релизов, со снимками кода каждые несколько месяцев, он скорее всего ориентирован на разработчиков, когда как Libreboot специально сделан для конечных пользователей. По-другому, цель Libreboot - *просто работать*. Конфигурация и установка coreboot также возможна, но Libreboot делает процесс *намного* проще.

Libreboot дает вам [свободы](https://writefreesoftware.org/), которые вы наверняка не получите с другими загрузочными прошивками, плюс более быстрый запуск ОС и [лучшую безопасность](docs/linux/grub_hardening.md). Он невероятно мощный и [настраиваемый](docs/maintain/) для многих случаев. Если вас не устраивают ограничения (также стоит упомянуть проблемы с безопасностью), наложенные на вас вашим поставщиком проприетарного BIOS, то Libreboot один из возможных вариантов для вас. Поскольку он наследует coreboot, в его коде нет никаких известных бэкдоров, и он не содержит устаревших ошибок 1980-х годов. Libreboot обеспечивает гладкую и быструю загрузку систем Linux/BSD на основе coreboot, которая регулярно проверяется и совершенствуется.

Libreboot надежнее многих проприетарных прошивок. Многие люди используют проприетарные загрузочные прошивки, даже если они используют [свободную операционную систему](https://www.openbsd.org/). Проприетарные прошивки часто [содержат](faq.html#intel) [бэкдоры](faq.html#amd) и множество багов. Проект Libreboot был основан в декабре 2013 года, с целью сделать coreboot доступным для людей, отдаленных от технологий.

Libreboot ориентирован на сообщество, с фокусом на помощь людям перестать пользоваться проприетарными загрузочными прошивками; мы сами хотим жить в таком мире, где все программное обеспечение [свободное](https://writefreesoftware.org/), и поэтому, Libreboot - это попытка приблизиться к этому миру. В отличие от больших поставщиков, мы не пытаемся задушить вас любыми способами, мы тажке не видим в вас никакой угрозы; мы уважаем возможность использовать, изучать, модифицировать и распространять программное обеспечение без ограничения и считаем, что все должны иметь это право. Что касается компьютеров, это вещи, которые вы купили, и у вас есть право изменять их как вам угодно. Когда вы слышите, что Intel разговаривает о *Boot Guard* (который мешает coreboot, разрешая запускать только прошивку, имеющую их подпись) или других крупных производителей, налагающих похожие ограничения, и вы слышите о "безопасности", они только говорят о *своей* безопасности, но не вашей. В Libreboot все наоборот: мы видим Intel Boot Guard и похожие технологии как покушение на ваши свободы и собственность (ваш компьютер), и поэтому наша миссия - помочь вам [вырвать](https://trmm.net/TOCTOU/) контроль обратно.

Libreboot это не форк coreboot
------------------------------

<img tabindex=1 class="r" style="max-width:25%;" src="https://av.vimuser.org/uboot.png" /><span class="f"><img src="https://av.vimuser.org/uboot.png" /></span>

По факту, Libreboot пытается быть как можно более похожей на *стоковый* coreboot для каждой материнской платы, но с множетсвом доступных опций для конфигурации, предоставляемых автоматической системой сборки.

Также, как и *Alpine Linux* - дистрибутив Linux, Libreboot - *дистрибутив coreboot*. Если вы хотите создать ROM образ с нуля, вам нужно быть экспертом в конфигурации coreboot, GRUB и любого другого программного обеспечения, которое может вам понадобится. С *Libreboot*, вы можете буквально скачать все с Git или архива с исходным кодом и запустить простой скрипт, которой и соберет весь ROM образ. Автоматизированная система сборки, названная `lbmk` (Libreboot MaKe), собирает ROM образы автоматически, без какого-либо пользовательского ввода и без каких-либо манипуляций, требуемых от пользователя. Все необходимая настройка уже была выполена заранее.

Если бы вы собирали обычный coreboot, не использую систему автоматической сборки Libreboot, вам бы потребовались специфические знания, для того, чтобы получить работающую конфигурацию.

Регулярные бинарные релизы Libreboot предоставляет эти ROM образы уже собранными, и вы просто можете их установить, без каких-либо дополнительных знаний или навыков, исключая способность следовать [упрощенной инструкции, написанной для пользователей, не обладающими техническими знаниями](docs/install/).

### Как помочь?

[Страница задач](tasks/) показывает задачи, над которыми нужно работать. Она будет обновляться по мере того, как задания будут выполнены/добавлены. Если вы хотите помочь, выбирайте одно из заданий и работайте над ним. 

<img tabindex=1 class="l" style="max-width:15%;" src="https://av.libreboot.org/hp8200sff/grub_open.jpg" /><span class="f"><img src="https://av.libreboot.org/hp8200sff/grub_open.jpg" /></span>

*Больше всего* вы можете помочь, *добавив* новые материнские платы в Libreboot, отправя ваш конфигурационный файл. Все, что поддерживает coreboot, может быть интегрировано в Libreboot с помощью ROM образов, предоставляемых в релизе. Смотрите:

* [Подайте заявку на сопровождающего/тестировщика платы](docs/maintain/testing.md)
* [Руководство по портировании на новые платы](docs/maintain/porting.md)
* [Документация по системе сборки Libreboot](docs/maintain/)

После следует обслуживание системы сборки (см. выше) и *документация*, которую мы воспринимаем очень серьезно. Документация очень важна в любом проекте.

*Поддержка пользователей* также важна. Оставайтесь в IRC, и если вы компетентны в том, чтобы помочь кому-то с их проблемой (или достаточно хитры, чтобы учиться у них), это отличная услуга для проекта. Многие люди также обращаются за поддержкой в раздел `r/libreboot` на Reddit.

Вся дискуссии насчет разработки и поддержка пользователей осуществляется через IRC. Больше информации можно найти на [странице контактов](contact.ru.md)

### Требуется помощь с переводом libreboot.org

Сейчас Libreboot перевел веб страницы на украинский и французский (но не все страницы).

Если вы хотите помочь с переводом, обновляйте существующие переводы и отправляйте нам переведенные версии. Для инструкций посетите:

[Как отправлять переводы для libreboot.org](news/translations.md)

Даже если кто-то уже работает над переводом на определенным языке, мы всегда можем использовать несколько людей. Чем больше, тем веселее!
