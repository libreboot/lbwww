% New Hampshire (USA) may soon enshrine Software Freedom into law. YOUR HELP IS NEEDED!
% Leah Rowe
% 8 January 2022

Introduction
============

This event of such global importance to libre software projects, and the
libre movement as a whole, has made me decide to write an article. **The
events in question, covered by this article, will occur on 11 January 2022.
This is just three days away from today, 8 January 2022 when this article was
written, so if you make a decision, you should make it now, today, and prepare.
Please continue reading.**

If you live in New Hampshire or in one of the neighbouring states, especially
Massachusetts, please listen up! If you are further away and unable to reach
New Hampshire all that easily, please spread the following news anyway. It's
important. As alien as it may seem to many of my readers, I'm actually writing
parts of this article as though someone who has never heard of Libre Software
(often referred to as *Open Source Software*) is
reading it, because I expect precisely that such people *will* read this
particular article.

When we say libre software, we mean software that gives people the
[freedom](https://writefreesoftware.org/) to
freely study, adapt, share, use and re-use all code or documentation, so as to
enable the free exchange of ideas and, simply speaking, democracy. This is your
Linux distros, BSD projects, and accompanying software that typically comes
packaged. It is sometimes referred to as *Open Source Software*. The word libre
is Spanish for "liberty", meaning freedom. With such software, you, the user,
are able to control your own computing according to your own priorities, which
you otherwise would not be able to do.

The opposite of libre software is called *proprietary software*. The purpose
of Libreboot is to help users *avoid* proprietary software at the firmware
level, whenever feasible.

What's happening in New Hampshire?
==================================

An important bill is being proposed in New Hampshire, which would enshrine
much of what we know as Open Source *into law*. Here is the proposed bill,
technically named "HB1273":\
<https://gencourt.state.nh.us/bill_status/legacy/bs2016/billText.aspx?sy=2022&id=1363&txtFormat=html>

You can read it for yourself, but here is a paraphrasing of what it proposes:

* *Specifically* bans state-run websites from serving proprietary javascript to
  clients
* Creates a commission to provide oversight, watching the use of libre code  by state agencies
* Bans state agencies from using proprietary software - maybe this could include schools, in the future!
* If a person is tried in a criminal case, they have the right to audit the source code of any proprietary software that collects evidence against them
* Encourages data portability (able to transfer data from one program to another)
* Bans certain non-compete clauses and NDAs (non-disclosure agreements) pertaining to Libre Software projects
* Bans state/local law enforcement from assisting with the enforcement of copyright claims against libre software projects
* Bans state agencies from purchasing proprietary software if libre software exists, for a given task

However, this is only a short summary. You are advised to read the bill in
detail. It's not very long.

At first glance, it may not seem that the bill affects individuals, but don't
be fooled; this is a hugely positive step forward for everyone! If the state is
using Libre Software, that most likely means it'll be used in education aswell.

Although perhaps not immediately and readily apparent, this is a stake in the
heart of proprietary software's current dominance, because it would remove one
key element of its attack against us; its abuse of education services.

If education services are using Libre Software, that means they'll probably have
children (the ones being educated) using it too. This is a *huge* step, and it
will result in more Libre Software developers in the future. Libre Software will
become more and more mainstream to the masses, which can surely only be a good
thing!

Freedom is always superior. The more people that have it, the better off we all
are, because freedom is also collective; it relies on others around us also
having it, so that we can defend each other. If more people have it, especially
if it results in more Libre Software developers in the future, that's one thing,
but imagine if *more* states like what they see and start to copy the new
legislation.

Now imagine that countries besides the US start doing it, inspired by the US's
success (and I think it will be a resounding success).

Imagine a world where such liberties over software are commonplace, actually
the default everywhere! Imagine a world where it's considered as important as
the ability to freely learn mathematics, or physics, as required reading
material in schools. *Imagine a world where any five year old can install a
libre operating system such as Linux/BSD, and Computer Science is mandatory in
schools from a young age. Imagine filing your tax returns with Libre Software,
exclusively. Imagine not even thinking about that, because it became the norm.*

*Imagine a world where proprietary software doesn't exist, because it is
obsolete; entire generations of people are taught to value freedom, and to
staunchly defend it, helping each other learn and grow (and produce better
software in the process, with less bugs, because people are now free to do
that, without relying on some evil company).*

Imagine a world where you're no longer being spied on because NSA, Apple and
Microsoft no longer have backdoor access to your computer. *Imagine having the
ability to say no, because that's what freedom is. Try to imagine it!*

One of our biggest problem has been simply that schools and governments do not
teach people about free computing. The right to learn, the right to read and
the right to hack. Our governments are made up of human beings just like you or
me, and they can be bought/corrupted; Microsoft, Apple and many others (such as
IBM) have done this for years, having the national infrastructures governing us
run on their proprietary systems, instead of systems that respect freedom; it
is essential that these systems run libre software, because a free and democratic
society should expect nothing less. Those companies buy influence *and they own
your politicians*.

All of this could change very soon. Something is happening in New Hampshire,
which could redefine our movement and give *libre software* real power
instead.

HOW TO HELP
===========

TESTIFY IN SUPPORT OF THE BILL
------------------------------

**The reading of the bill is happening on 11 January 2022. This is when you
should go to New Hampshire.**

**Location of hearing: Legislative Office Building in Concord, New Hampshire:\
<https://en.wikipedia.org/wiki/New_Hampshire_Legislative_Office_Building>**

The organizer of the proposed bill, *Eric Gallager*, has left instructions on
Twitter. The following is a *nitter* link, which lets you view the relevant
Twitter thread without running libre Javascript in your browser:\
<https://nitter.net/cooljeanius/status/1479663133207764992>

Further instructions for what room to go to, when you get there:\

See Nitter link:\
<https://nitter.net/cooljeanius/status/1479062316532604930>

(original twitter link: <https://twitter.com/cooljeanius/status/1479062316532604930>)

**Please read both threads very carefully!**

**YOU NEED TO GO TO NEW HAMPSHIRE IN PERSON!**

If you're able to go to New Hampshire to attend the reading of the bill, please
do so! Voice your support of the bill, and say why you think it's important.

Tell the lawmakers that you demand freedom!

This thread on Twitter is where Eric announced that the reading of the bill is
to proceed (original Twitter URL):\
<https://twitter.com/cooljeanius/status/1479555737223413760>

More states/countries will follow
---------------------------------

If this bill is passed in New Hampshire, more states will likely follow. It
will lead to a massively renewed drive to liberate all computer users, and US
laws tend to be copied/pasted around the world too.

This bill, if passed, will have a hugely positive impact on Libre Software at a
global level.

You *must* support this bill. If you want to see it pass, please go to New
Hampshire on 11 January 2022 to make sure your voice is heard.

OUR ENEMIES WILL BE THERE
-------------------------

The *proprietary* software companies like Microsoft and Apple will also be
there, trying to argue the case *against* the use of Libre Software.

There is already precedent; please watch this video, which shows how Microsoft
(for example) might behave in the reading of the bill. This video is from a
discussion within the European Union, several years ago:\
<https://yewtu.be/watch?v=W_S0k1sx8EM> (invidious link. works without
javascript enabled, if you wish)

They will try to trick the law makers by claiming things such as:

* **"Open Source is insecure / you will get hacked"** - nothing could be
  further from the truth! Free operating systems such as Linux, FreeBSD and
  especially OpenBSD, are among the most secure operating systems available.
* **"Open Source is used by criminal hackers"** - here, they use the
  term *hacker* to describe someone who illegally gains access to someone
  elses computer. Don't fall for it. Maintainers of libre operating systems
  like Linux distros or the BSDs are actively working to make the internet
  and computers in general *more secure*
* **"Software authors deserve to be paid!"** -  In fact, many libre software devs
  are *paid* to work on Open Source! Many companies, including big ones,
  work on it. There are also hobbyists or otherwise unpaid people, who might
  work on Libre Software for a number of reasons (wanting to make the world a
  better place, wanting the glory of recognition for solving a major problem,
  and more often than not, simply because *it is fun to do so and you make a
  lot of friends too!*) - No, these companies (e.g. Microsoft) are only arguing
  in reality for the ability to pay their *shareholders*, and they control the
  software exclusively. In fact, libre software has repeatedly and consistently
  over the years *defined* the computing industry, creating all kinds of new
  employment opportunities; for example, docker is widely used today and it is
  libre software, used by millions of companies for commercial gain, and the
  apache web server revolutionized the web back in the day, enabling lots of
  ISPs to easily host websites - many of the common protocols that we depend
  upon today, that businesses depend upon (and get paid to maintain or provide
  services/support for) are in fact free as in freedom!
* **"Developers should get recognition for their work"** - in libre software, you
  can easily make a name for yourself with relatively few resources except your
  own computer and an internet connection, plus some cheap hosting. When most
  developers work on *proprietary* software such as Windows, they don't get
  recognition; their copyright is assigned to their employer (e.g. Microsoft)
  who will take all the credit!
* **"Free software is unreliable / costly to maintain"** - actually, it has been
  well known for years that libre software is generally more stable and reliable
  than proprietary. In cases where it isn't, it is quickly improved, and in
  complete freedom. Free software has a lower cost to maintain and service, and
  you have a free market where you can choose who you hire to write/maintain it
  for you (if you won't do that yourself); meanwhile, proprietary software
  such as Windows is often full of bugs, crashes often and there is only one
  provider of support most of the time, who will charge a heavy price, while
  also charging a lot of money for the software itself - libre software
  is *free as in freedom*, but also usually *free as in zero price*.
* **"Libre software comes from potentially untrustworthy sources"** - This is
  pure nonsense, because the very freedoms provided by libre software (access
  to source code, ability to work on it yourself, and see what others did)
  means that people generally do not add malware to public software sources,
  because they'd be discovered instantly. *Distributions* of Linux and
  other free operating systems are often maintained by many people, who verify
  the safety of each software package that they provide; they are also usually
  provided by each *distro*, in a central repository unlike with, say, Windows
  where you really *are* randomly executing binaries from all kinds of
  locations (often even without checking the cryptographic checksums of those
  files, to verify their integrity). It's very hard to become infected with
  malware on a free system, precisely because security is handled much better;
  the design of unix-like operating systems in particular is also naturally
  more secure, due to better separation of root/user privileges.
* **"Libre software isn't controlled, and is unknown."** - this is completely
  false. These non-libre software companies are only talking about *their*
  control, and it's quite telling that they completely disregard yours, in this
  very sentence. In fact, Libre Software *is* controlled, but it's not controlled
  by some external entity; *your* installation of libre software is controlled
  by *you*.

If you're familiar with the *Matrix* films, proprietary operating systems like
Windows/MacOS are basically like the Matrix; bland, no individuality, no
independent thought, everything tightly controlled. By contrast, libre operating
systems (such as Linux distributions or the BSDs) are like zion/io; vibrant,
full of life, buzzing with activity, everything loose and free, and everyone
is different (a highly diverse culture of people from all walks of life, acting
in common cause but nonetheless individuals).

Meanwhile, Windows is known to have backdoors. Microsoft actively informs the
NSA about how to exploit them, so that it can break into people's computers
and steal private data.

Proprietary software companies are evil, and must be opposed. They know that
if this bill passes, their days are numbered.

Defend freedom! Don't listen to any of the arguments against it by proprietary
software companies; they don't care about you, and instead only care about
profit. They fundamentally do not want you to have any sort of freedom over
your own computer, and they actively pursue tactics (such as DRM) to thwart you.

Microsoft and Apple are not your friends. There is no such thing as the
Windows community. When you use proprietary systems, you are isolated from
everyone around you, and so are they. *You* are the product, for the proprietary
software to exploit at the behest of their developers who only care
about *money*.

However, there *is* such a thing as the Libre Software community. It is a
vibrant community, consisting of millions of people collectively all over the
world, and they are all free to work with each other infinitely. It gave us
most of the technology that we take for granted today, including *the modern
internet, where ISPs run libre software almost exclusively!*

