% New Hampshire once again on the cusp of enshrining Software Freedom into law. YOUR HELP IS NEEDED.
% Leah Rowe
% 12 February 2023

Free software in your government?
--------------------------

This article makes use of the term *libre software*, which has the same meaning
as more popular terms such as *open source software* 
or *[free software](https://writefreesoftware.org/)*
or *free and open source software*. More information can be found about
it [here](https://en.wikipedia.org/wiki/Open_source) - to the Libreboot
project, this is important because it talks about your *freedom* to study,
adapt, share and re-use software as you see fit, alongside the rest of
humanity in a collective development effort, as opposed to the alternative
where we would be restricted by companies like Microsoft or Apple, who only
care about *controlling us* to make money.

You may recall last year's article: [New Hampshire (USA) may soon enshrine
Software Freedom into law](usa-libre.md) - a proposed bill, if it passed,
would have provided official legal protections in favour of libre software in
the state of New Hampshire, in the United States. The bill didn't pass, largely
because of a complaint that the bill was too all-encompassing, and so the idea
then was that the bill should be split into a series of smaller bills that, in
combination, achieve the same goals.

Since then, Eric Gallager (the representative behind the original bill) in
New Hampshire has done exactly that, and a new hearing takes place very soon,
on the *16th of February, 2023, at 1PM*.

I once again call to action, any person that lives in New Hampshire or the
surrounding states in the USA. Your participation could help secure the rights
of all libre software users and developers, well into the future. I myself do
not live in the US, so I'm hoping that my American readers will listen well to
what I have to say.

With your help, libre software could suddenly find itself in a much stronger
position, with more users and more developers, encouraged by such positive
changes.

### When, who, what and where?

Eric Gallager, the representative behind the previous bill, has continued his
efforts and now has a new hearing for the following bill very soon:

* **16 February 2023, 1PM**: prohibiting, with limited exceptions, state
  agencies from requiring use of proprietary software in interactions with the
  public (house bill: HB 617-FN)

The text of the proposed bill can be read here: \
<https://gencourt.state.nh.us/lsr_search/billText.aspx?id=188&type=4>

It is critical that as many people show up as possible, to express support for
the bill, and to defend it against any opposition.

**Location of hearing: Legislative Office Building in Concord, New Hampshire:\
<https://en.wikipedia.org/wiki/New_Hampshire_Legislative_Office_Building>**

The bill's hearing shall take place in room 306-308.

### Who to contact

Eric Gallager is the representative in charge of the proposed bill, and you can
contact him in the following ways:

Email address: \
[Eric.Gallager@leg.state.nh.us](mailto:Eric.Gallager@leg.state.nh.us)

Mastodon page: \
<https://social.treehouse.systems/@egallager>

Twitter page (use of Twitter is ill advised, due
to its proprietary nature - use Mastodon or email if you can): \
<https://twitter.com/cooljeanius>

Why should you support this bill?
------------------------------

If this newly proposed bill is passed, it will provide the libre software
movement a *foot in the door*, that could lead to greater reform at a later
date, and strengthen the entire movement. This is because of the knock-on
effect it would have: as more people benefit from it, more states (in the US)
and countries outside of the US may follow, implementing similar laws.

Laws are often made that reduce our freedoms. If any law should be passed, it
should be a law that strengthens or otherwise reaffirms civil liberties, which
is what the proposed bill aims to do.

Libre software is about civil liberties, as it pertains to computer science,
because of the *right to learn* and the *right to read*. Just as mathematics
or physics should be free for anyone to study and make use of, so too should
that be the case for computer science.

Any civil liberties that we have today are the result of *laws* that protect
them, because the purpose of law is to provide punishment for violation; if
no law exists, for or against something, then no punishment can take place.
For example, most countries have a law that says you should not be robbed; if
someone then robs you, then they get punished with jail time. The right to
private property is an important right.

If no laws exist that protect libre software projects, then they are
vulnerable.

Just as you should have the right to property, you must also have the right to
pursue a happy, productive life - the right, in practise, to work on libre
software should be part of that, if computer science is something you're
interested in.

This right *also pertains to property*, specifically the property that is your
computational devices; tablets, PCs (desktops, laptops), whatever you use.
Libre software lets you truly *own* your computer, because it doesn't leave
you beholden to a *licensor*. You have the right to study, adapt, share and
re-use the software infinitely, and other people also have this right. With
libre software, your computer is no longer a *product* for a specific purpose
per se, but rather, a general purpose machine that can be reprogrammed
for *any purpose as you see fit*. This is similar to your right to have your
car modified, or repaired by anyone, including you, or perhaps your right to
reorganise (even completely re-build) your house.

The proposed bill's hearing, on the 16th, regards the usage of proprietary
software by state agencies. *State agencies* could include schools run by the
state, but it could also include things like your local tax office. When you
file taxes, it is often the case that it cannot be done without running some
proprietary software, and many schools will insist that their students use
Windows (or other proprietary OS) rather than, say, Linux or BSD. By mandating
in law that people should be able to use *libre* software, it will create a
level playing field, because the state would *have to* make accomodation for
libre software; while initially a burden (time and money spent), it would be
a huge social boon later on, because if states stop relying on proprietary
software licenses, the money they currently spend on that can instead be spent
elsewhere, or on paying programmers, providing *better software* to the public;
the actual overall cost may be exactly the same as today, but with more
benefits for everyone, not to mention greater freedoms for computer users.

To put it simply: the right to your (computational) property would be enhanced
by this bill's passage, by forcing the state to support libre software, against
the whims of proprietary software vendors. This, like anything that strengthens
the libre software movement, would increase the likelihood that you can actually
use libre software, on computers that you buy in the future, which means that
your *right to your own property* is enhanced, because at the end of the day,
that's exactly what *software freedom* is all about.

We can all agree about education. The idea that people should be able to learn
and grow as people is natural, and we all want to better ourselves. Libre
software is an important part of that, in the field of computer science.
We can also agree about private property.

What about money? Is this not an important part too? Is it not true that, with
private property, you can also have *private enterprise*?

Libre software is very *profitable*, to a far greater extent than proprietary
software. When the free exchange of ideas and knowledge is permitted to flow,
innovation is much more likely. We can talk all day about the right to education
but money matters too. Many software developers *learn* on libre software,
because that is the only way to get really good with computers. You can't become
a competent programmer by using Windows or MacOS. Linux or BSD are your only
real choice, because that's where all the interesting development happens.
Today's most profitable industries are powered by libre software; without
it, we would be living in a very different world today, locked down by the
likes of Microsoft or Apple who see software as a *product*, a *means to an
end*, rather than the end in itself, that end being knowledge and
self-empowerment. Technology has given us many freedoms today, thanks to the
tireless efforts of libre software developers everywhere.

For example: nearly every website you visit runs on some Linux or BSD system,
probably running the Apache web server, or (much more common nowadays) nginx.
Windows just doesn't scale as a *server* OS, it is completely inflexible, but
linux and bsd systems can be tweaked to do whatever you want.

When I say *libre software is profitable*, I'm not referring to Microsoft
or Apple's profits. No, I'm referring to *yours*. With libre software, *you*
have the freedom to make real money; I'm just one of many examples of people
who do just that. With *software freedom*, you can take existing technology
and build something completely new that becomes the Next Best Thing; everyone
else has this freedom as well, and people share knowledge freely because of the
culture that type of world inspires. It's the world we live in, now.

The people of New Hampshire will benefit greatly, if such freedoms are
enshrined in law. It will be a huge success, and it will lead to more
jurisdictions (both within and outside the US) to follow suit. It will lead to
the *end* of monopoly powers like Microsoft or Apple, completely opening up
the entire body of knowledge to everyone, because that will become the norm;
hoarding knowledge will become *unprofitable*, because the new culture would
simply not allow it, so companies like Microsoft and Apple, if they want to
remain relevant, would have to start releasing more source code themselves,
rather than keeping everything proprietary. The people of the world will stop
seeing them as a gold standard; they are not, and have never been. Libre
software has always been superior, in every way, to proprietary software,
because it allows you to actually *own* the computer you bought, in practise
and in spirit.

Proprietary software is like renting a house; you have a license to use it,
but you don't have the freedom to really change anything, and the licensor
(equivalent to a landlord) can pull the plug at any time. Libre software, on
the other hand, is equivalent to owning a house. When you run all libre software
on your computer, *you* control that computer. You can modify whatever you want,
or pay whoever you want to do that for you, to maintain everything for you,
and you can share your work with others, something which other people already
do; it's the reason libre software is so successful, precisely that people
share their knowledge.

The state is an important part of our lives, no matter which country or
jurisdiction we live in. We interact with it constantly, for services that we
all rely on, so it is important that we should be *able to* with the software
of our choice. This is why it's so important that the state, in any
jurisdiction, take steps to ensure that libre software users don't get left
behind. If passed, this will will strongly reinforce the *right* of computer
users everywhere, to their own computational property.

Please, please please if you can take time out of your day, then please show
up to to defend this bill and make sure that it gets passed!
