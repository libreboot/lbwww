% Libreboot mailing list and git mirror now available on sourcehut (sr.ht)
% Leah Rowe
% 11 February 2024

Exactly as the title suggests, Libreboot now has git hosting on
sourcehut. See:

* <https://git.sr.ht/~libreboot/lbmk>
* <https://git.sr.ht/~libreboot/lbwww>

As of this time, it's just lbmk and lbwww, but more repositories may be
mirrored there at some point.

There is also a mailing list:

* Archives: <https://lists.sr.ht/~libreboot/libreboot>
* Subscribe: [~libreboot/libreboot@lists.sr.ht](mailto:~libreboot/libreboot@lists.sr.ht)

Patches may be sent via the mailing list, though codeberg is still considered
the main provider for that (which uses the forgejo software, not sourcehut).

There is just one mailing list, which will be used later for project
announcements (complementing the regular news page and RSS feed
on libreboot.org). It can also be used to ask questions, if you wish - though,
there is also IRC and r/libreboot. This single mailing list will be used for
everything, though it may be split into other mailing lists in future. Running
just one mailing list is more efficient, for a project at Libreboot's scale.

More information is available on the [contact page](../contact.md).

That is all.
