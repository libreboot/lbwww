% HP EliteBook 820 G2 support added to Libreboot!
% Leah Rowe
% 10 January 2024

<div class="specs">
<center>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2.jpg" /></span>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_lid.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_lid.jpg" /></span>
<br/>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_ports1.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_ports1.jpg" /></span>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_ports2.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_ports2.jpg" /></span>
<br/>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_backlit.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_backlit.jpg" /></span>
<img tabindex=1 alt="HP EliteBook 820 G2" class="p" src="https://av.libreboot.org/hp820g2/hp820g2_inside.jpg" /><span class="f"><img src="https://av.libreboot.org/hp820g2/hp820g2_inside.jpg" /></span>
</center>
</div>

This patch was uploaded last night, adding support for HP EliteBook 820 G2
to Libreboot:
<https://browse.libreboot.org/lbmk.git/commit/?id=401c0882aaec059eab62b5ce467d3efbc1472d1f>

Full hardware specifications can be found on HP's own website:

<https://support.hp.com/gb-en/document/c04543492>

Free software BIOS
------------------

This is a beastly 12.5" Broadwell machine from HP, the main benefit of which is
greater power efficiency (compared to Ivybridge and Haswell platforms), while
offering similar CPU performance but much higher graphics performance.

Variants exist with either Intel Core i5-5200U, i5-5300U, i7-5500U or
i7-5600U and it comes with a plethora of ports; 3x USB 3.0, DisplayPort (which
can do 4K 60Hz), a VGA port, can be expanded to 32GB RAM, has *3* slots which
can take SSDs (PCIe, M2 and regular SATA), also has a side dock connector (for
a docking station). The screen is eDP type and can be upgraded to 1920x1080.

Full specifications can be found on HP's website:
<https://support.hp.com/gb-en/document/c04543492>

This is a nice portable machine, with very reasonable performance. Most people
should be very satisfied with it, in daily use. It is widely available in
online market places. This page will tell you how to flash it!

All variants of this motherboard will come with Intel HD 5500 graphics, which has
completely free software initialisation in coreboot, provided by *libgfxinit*.

How to install Libreboot
--------------------

Refer to the [HP 820 G2 install guide](../docs/install/hp820g2.md)
for more information about how to flash this board. This machine is widely
available online.
