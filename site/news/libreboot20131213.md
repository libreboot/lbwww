% Libreboot 20131213
% Leah Rowe
% 13 December 2013

r20131213 (2nd release) {#release20131213}
-----------------------------------------

-   13th December 2013

### Supported:

-   ThinkPad X60
-   ThinkPad X60s

### Development notes

-   added background image to GRUB2
-   added memtest86+ payload to grub2
-   improvements to the documentation
-   new grub.cfg

