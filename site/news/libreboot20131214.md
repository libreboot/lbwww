% Libreboot 20131214 release
% Leah Rowe
% 14 December 2013

r20131214 (3rd release) {#release20131214}
=======================

-   14th December 2013

Supported:
----------

-   ThinkPad X60
-   ThinkPad X60s

Development notes
-----------------

-   Added SeaBIOS payload to GRUB2 (for booting USB drives)
-   new grub.cfg

