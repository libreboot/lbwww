% Libreboot 20140605 release
% Leah Rowe
% 5 June 2014

Revision notes (5th June 2014):
-------------------------------

-   added backlight support (Fn+Home and Fn+End) on X60
-   fixed broken/unstable 3D when using kernel 3.12 or higher
-   (see 'BACKPORT' file)
