% Dell Latitude E6400 added to Libreboot (blob-free, no disassembly)
% Leah Rowe
% 19 April 2023

**UPDATE (9 May 2023): Libreboot confirmed working on variants such as
[E6400 XFR, and the Nvidia GPU variant is now supported](e6400nvidia.md).**

Free as in freedom!
------------------

Today, Libreboot gained the Dell Latitude E6400 laptop port. This is a
blob-less port, courtesy of Nicholas Chin (`nic3-14159` on Libreboot IRC).
Nicholas has worked extensively on this port, for several years, and it's in
a ready state for entry to Libreboot.

The hardware platform is GM45, similar to ThinkPad X200, T400 and so on that
Libreboot already supports.

You can learn more on the [Latitude flashing guide](../docs/install/latitude.md)

### 100% libre, blob-free

This is a *blob-free* board in the boot flash. No Intel ME firmware needed,
and [microcode can be removed if you wish](gm45microcode.md) (you should still
leave microcode there, as is default, but some people remove it by choice that
we give them - see: [Binary Blobs Reduction Policy](policy.md)).

*But wait.* There's more. A lot more of *them*, that is.

### Readily available on eBay etc, and cheap

Dells were much more popular than those ThinkPads, and more commonly used,
so there are still *several* of these available on sites like eBay. Enough to
keep people with an affinity for GM45 machines happy for a while longer (older
GM45 ThinkPad X200, T400 etc are very hard to find nowadays).

This could very well replace X200, T400 etc, in terms of what certain people
want to use - nice enough screen/keyboard, and easy of installation just makes
this a very nice machine indeed.

But wait.... It gets better:

### Software flashing possible! (no disassembly)

**NOTE (15 October 2023): The util is now called `dell-flash-unlock`, but it
was previously called `e6400-flash-unlock`. Links have been updated. And
Nicholas is still a genius.**

tl;dr Nicholas is a genius, but he spent time studying the board, finding that
the EC is hooked up to GPIO33 which allows for flash descriptor override. He
successfully reverse engineered a command that can be used to disable IFD
protections, and discovered that the SMM BIOS lock protection could be
bypassed, allowing installation of Libreboot.

This is without needing to disassemble. No clip required.

**That is to say, you can install Libreboot on this board without
taking it apart, and you can install it easily within 5 minutes.**

This is done with the following utility from Nicholas Chin, which I merged
into lbmk:

<https://browse.libreboot.org/lbmk.git/tree/util/dell-flash-unlock>

The original util, before Nicholas sent it to lbmk, is here (same util):

<https://gitlab.com/nic3-14159/e6400-flash-unlock/-/commits/base>

It merges some code changes that I made myself, after Nicholas published it,
tidying up the code a bit (OpenBSD-like coding style adopted, for fun). See:

<https://codeberg.org/vimuser/e6400-flash-unlock/commits/branch/puffy>

Libreboot users should use the one in `util/` on Libreboot proper.


That is all! Read the manuals to know more about this machine. Thank you!
