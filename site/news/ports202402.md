% NEW BOARDS: Dell OptiPlex 7020 9020 SFF/MT, HP EliteBook 8560w and more Dell Latitudes
% Leah Rowe
% 21 February 2024

Libreboot is a free/open source BIOS/UEFI replacement, providing boot firmware that initialises the hardware in your computer, to then load an operating system (e.g. Linux/BSD). It provides many additional benefits such as fast boot speeds, greater security and greater customisation, but the *primary* benefit is [software freedom](https://writefreesoftware.org/learn).

Libreboot has had a slew of new motherboards added recently. The work doesn't
stop. Many more will be added, for the next Libreboot release.

The boards are:

Dell OptiPlex 7020 and 9020 (MT and SFF)
-----------------------------------------

See: [Dell OptiPlex 9020 SFF/MT (and 7020)](../docs/install/dell9020.html)

This is a highly performant Haswell board, with plenty of expansion and
upgrade options available. Notably, it is a *desktop* computer, available
as *SFF* (similar to ITX) and *MT* (ATX) variants.

It is also also sold with [Libreboot preinstalled](https://minifree.org/product/libreboot-9020/);
I sell these machines preinstalled, as well as several others, to raise funds
for the Libreboot project. Profits from Minifree sales directly fund the
Libreboot project. I added this myself recently, based on patches from coreboot
Gerrit.

HP EliteBook 8560w
------------------

Riku Viitanen added this recently. See:
[HP EliteBook 8560w](../docs/install/hp8560w.html).

Of note: this board uses an *MXM* graphics card, which you can upgrade. Riku
added support for loading MXM configuration via INT15H interrupt in SeaBIOS,
and he wrote a tool ([mxmdump](https://codeberg.org/Riku_V/mxmdump/)) that can
extract the config.

As per MXM specs, the MXM config may be hardwired in a ROM on the i2c bus, and
we don't need to do anything there except load the VGA option ROM from SeaBIOS.
However, on these EliteBooks, it must be provided in the boot flash.

A *lot* more Dell Latitudes
---------------------------

Nicholas Chin recently added these Dell Latitudes (of Sandybridge and Ivybridge
platforms) to Libreboot:

* [Dell Latitude E6420](../docs/install/e6420.html)
* [Dell Latitude E5520](../docs/install/e5520.html)
* [Dell Latitude E5530](../docs/install/e5530.html)
* [Dell Latitude E6520](../docs/install/e6520.html)

These are flashable via the usual method, same as E6400 and E6430.

I'm extremely happy with this great progress. These and more will be available
in the next Libreboot release, but if you want to use them now, you
can [build Libreboot from source](../docs/build/) and install them using the
available flashing instructions.

Enjoy!
