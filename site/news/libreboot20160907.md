% Libreboot 20160907 release
% Leah Rowe
% 7 September 2016

In comparison to Libreboot 20160902:

For existing boards, there are no new board specific changes.

This release adds one new motherboard to libreboot:

-   Intel D945GCLF desktop motherboard (thanks to Arthur Heymans)

Other bugfixes:

-   Various improvements to the documentation
-   re-added "unset superusers" to the grub.cfg, which was needed for
    some users depending on the distros that they used
