% Libreboot 20140611 release
% Leah Rowe
% 11 June 2014

Revision notes (11th June 2014):
--------------------------------

-   removed 'CD' boot option from coreboot.rom (not needed)
-   removed 'processor.max\_cstate=2' and 'idle=halt' options (see
    README.powertop file)
