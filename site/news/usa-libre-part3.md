% NEW New Hampshire software freedom bill hearing on 21 February 2023 needs your support!
% Leah Rowe
% 20 February 2023

Introduction
============

You may recall last year's article: [New Hampshire (USA) may soon enshrine
Software Freedom into law](usa-libre.md) - a proposed bill, if it passed,
would have provided official legal protections in favour of libre software in
the state of New Hampshire, in the United States.

Last year's bill didn't pass, so Eric Gallager, the person behind it, started
splitting it into smaller bills that are more likely to pass; this recently
cultimated first in [house bill 617-FN](usa-libre-part2.md) for which there was
a hearing.

Now, there is another upcoming bill hearing: house bill 556-FN:\
<https://gencourt.state.nh.us/lsr_search/billText.aspx?id=193&type=4>

It is critical that as many people show up as possible, to defend the bill
and advance the cause of *[software freedom](https://writefreesoftware.org/)*.

*New Hampshire residents: In addition to showing up on the day, you can also
contact your house representative, and ask them to support this bill! This
web page can let you find who your representative is:\
<https://www.gencourt.state.nh.us/house/members/>*

Can't attend?
-------------

That's OK! You can still help. Please tell as many people about this as
possible, and spread the news on as many websites/blogs as possible. Post it
on your social media account, if you have one. Write to the media!

When and where?
===============

**This bill's hearing is on 21 February 2023 at 9AM in the Legislative Office
Building, 33 N. State Ct., Concord, New Hampshire:**\
<https://en.wikipedia.org/wiki/New_Hampshire_Legislative_Office_Building>

Please, please if you can, show up to the hearing! New Hampshire needs *you*.
If this and similar bills pass, in New Hampshire, it will spread like wildfire
to many other states, and countries, bringing us many steps closer to a world
where *everyone* has the ability to use libre software *exclusively* - such is
the goal of the movement, and that of the Libreboot project which is a part of
said movement.

Who to contact
==============

Eric Gallager is the representative in charge of the proposed bill, and you can
contact him in the following ways:

Email address: \
[Eric.Gallager@leg.state.nh.us](mailto:Eric.Gallager@leg.state.nh.us)

Mastodon page: \
<https://social.treehouse.systems/@egallager>

Twitter page (use of Twitter is ill advised, due
to its proprietary nature - use Mastodon or email if you can): \
<https://twitter.com/cooljeanius>

What does house bill 556-FN say?
================================

The actual text of the bill is provided here:\
<https://gencourt.state.nh.us/lsr_search/billText.aspx?id=193&type=4>

If passed, the New Hampshire Information Technology Council would then be
required, by law, to promote software freedom to the Department of Information
Technology Commissioner. This would include analysis of practical issues such
as cost, and deployment strategies for libre systems to replace current
proprietary systems; more importantly, it would require them to study ways of
reducing (or eliminating) the need for citizens to use proprietary software
for interactions with the state. The bill also promotes the principle
of *copyleft* licensing, such as the GPL; regardless of how you feel about
copyleft versus permissive (BSD-style) licensing, the advancement of *any*
libre software on such massive scale will help the entire movement.

Live streams
============

Although not mentioned in previous articles on this agenda, the New Hampshire
court building provides live streams of hearings.

Link to New Hampshire courthouse youtube channel:
<https://www.youtube.com/@NHHouseofRepresentatives>

The last few software freedom bills were streamed on this one:

*House Executive Departments and Administration*

Per the text of HB-556, the stream for *that* bill will be on:

*Science, Technology and Energy*

The links change, for each stream, so you should check them on the day.

Use invidious!
--------------

The Youtube.com link is provided, above, for the sake of completion. However,
you should use an Invidious instance. Invidious acts as a proxy for Youtube,
preventing Google from sniffing your habits or collecting data, and it also
bypasses the need for running proprietary JavaScript code.

Here's a link to the New Hampshire court channel, on an *excellent* Invidious
instance (excellent in terms of uptime, and speed):

<https://piped.kavin.rocks/channel/UCxqjz56akoWRL_5vyaQDtvQ>
