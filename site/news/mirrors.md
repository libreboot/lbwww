% New Git repositories added as backup Libreboot mirrors
% Leah Rowe
% 11 April 2023

Libreboot's mission is to help as many people use coreboot as possible. This
means Libreboot should reach out to as many people as possible. By maintaining
a presence on *several* websites, it can do just that!

So today, I started making accounts on a few of the more popular Git hosting
platforms that seem *reasonable* from a
[software freedom](https://writefreesoftware.org/) perspective, or are
otherwise places where lots of freedom-minded people might see it. These
mirrors are provided as a *backup*, in case the main Codeberg mirror is ever
offline for any reason.

I can now announce the following *official* mirrors of Libreboot's git
repository, controlled directly by the Libreboot project; first, the build
system (lbmk):

* <https://gitea.treehouse.systems/libreboot/lbmk>
* <https://0xacab.org/libreboot/lbmk/>
* <https://framagit.org/libreboot/libreboot>
* <https://gitlab.com/libreboot/lbmk>
* <https://pagure.io/libreboot>
* <https://rocketgit.com/libreboot/libreboot>

Secondly, lbwww (markdown files for the Libreboot website):

* <https://gitea.treehouse.systems/libreboot/lbwww>
* <https://0xacab.org/libreboot/lbwww>
* <https://framagit.org/libreboot/lbwww/>
* <https://gitlab.com/libreboot/lbwww>
* <https://rocketgit.com/libreboot/lbwww>

All of these mirrors were created *today*, and they are listed on the Git
page. See:

[Libreboot git repositories / code review page](../git.md)

More mirrors will be created over time, and added as they become available.
If you know of a decent Git site where you think Libreboot should mirror on,
or you're the *operator* of such a site and want Libreboot on there, feel free
to [get in touch](../contact.md)!

UPDATE: on 12 April 2023, Libreboot gained these additional mirrors:

* <https://git.fosscommunity.in/libreboot/lbmk>
* <https://git.fosscommunity.in/libreboot/lbwww>

UPDATE: on 15 April 2023, Libreboot gained these additional mirrors:

* <https://git.disroot.org/libreboot/lbmk>
* <https://git.disroot.org/libreboot/lbwww>
