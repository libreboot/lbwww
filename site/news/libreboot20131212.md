% Libreboot 20131212
% Leah Rowe
% 12 December 2013

r20131212 (1st release) {#release20131212}
----------------------------

-   12th December 2013

### Supported:

-   ThinkPad X60
-   ThinkPad X60s

### Development notes

-   initial release
-   source code deblobbed
