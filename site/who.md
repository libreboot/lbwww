---
title: Who develops libreboot?
x-toc-enable: true
...

The purpose of this page is to clearly define who works on libreboot, who runs
the project, how decisions are made, and in general how the project functions.

You can find information about major contributions made to libreboot, on this
page which lists such people: [List of contributors](contrib.md)

Leah Rowe (founder, lead developer)
===================================

Leah Rowe is the founder of the libreboot project. Leah oversees all development of libreboot, reviewing
outside contributions, and has the final say over all decisions. Leah owns and
operates the libreboot.org servers from her lab in the UK.

Caleb La Grange
===============

Caleb goes by the screen name `shmalebx9`.
Caleb mainly deals with improvements to the lbmk build system, board porting,
and documentation.

Alper Nebi Yasak
================

Alper handles development of the U-Boot payload support in Libreboot,
coordinates testing on Chromebooks that use it, and does any necessary
upstream work on U-Boot itself. `alpernebbi` on Libera IRC.

List of contributions
=====================

You can learn more about any developer's involvement with Libreboot, by reading their
entry on the [page listing all contributors, past and present](contrib.md)

Developers wanted!
==================

**Learn how to contribute patches on the [git page](git.md)**

All are welcome to join in on development.
