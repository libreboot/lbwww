---
title: Libreboot logo license
...

The Libreboot logo is copyright 2014 Marcus Moeller, and it was also created by
that person. It is released under the terms of the Creative Commons Zero
license, version 1.0.

The sticker files, based on that logo, are made by Patrick McDermott in 2015,
released under the same license.

A copy of this license (CC-0 1.0) can be found at:
<https://creativecommons.org/publicdomain/zero/1.0/legalcode>

The font on the sticker designs is `lato`. Install this, otherwise the vectors
won't look correct for the text.

You can see the logo files here: <https://av.libreboot.org/logo/>
